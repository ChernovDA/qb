const pgp = require('pg-promise')();
const app = require('express')();
const httpServer = require("http").createServer(app);
const io = require("socket.io")(httpServer,{
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

const connectionString = `postgresql://qb:qb@192.168.100.4:5432/qb`;
// const connectionString = `postgresql://qb:##########@localhost:5432/qb`;

const db = pgp(connectionString);

io.on('connection', (socket => {
  console.log('new connect');
  let sco;

  db.connect({direct: true})
    .then(obj => {
      console.log('bd connected');
      sco = obj; // save the connection object;
      sco.client.on('notification', data => {
        console.log(data.payload);

      });

      // execute all the queries you need:
      return sco.none('LISTEN $1:name', 'qbdevchannel');
    })
    .then(data => {
      // success
      console.log('data', data);
    })
    .catch(error => {
      // error
      console.log(`error: ${error}`);
    });

  // db.none('NOTIFY $1:name, $2', ['qbdevchannel', 'my payload string'])
  //   .then(() => {
  //     console.log('Notification sent.');
  //   })
  //   .catch(error => {
  //     console.log('NOTIFY error:', error);
  //   });


  socket.on('insert-stages-template', (table, data, gameCode) => {
    console.log('insert-stages-template action');
    db.one('select id from games where code = $1', gameCode)
      .then((game) => {
        for (const row in data) {
          if (data.hasOwnProperty(row)) {
            // создаем набор карт
            db.one('insert into "stageBackground" ("posX", "posY", src) values ($1, $2, $3) returning id', [
              data[row].map.posX,
              data[row].map.posY,
              data[row].map.src,
            ])
              .then(map => {
                // создаем уровень
                db.one('insert into "stagesTemplate" ("stageModule","code","title","portalIcon","questionIcon", map) values ($1, $2, $3, $4, $5, $6) returning id', [
                  1,
                  data[row].stage,
                  data[row].title,
                  1,
                  1,
                  map.id
                ])
                  .then(stage => {
                    let columns = new pgp.helpers.ColumnSet([
                      'available',
                      'delayWarp',
                      'icon',
                      'moveTo',
                      'posX',
                      'posY',
                      'stage',
                      'game'
                    ], {table: 'portalsTemplate'});

                    let values = [];
                    for (const portal in data[row].portals) {
                      if (data[row].portals.hasOwnProperty(portal)) {
                        values.push({
                          available: data[row].portals[portal].available,
                          delayWarp: data[row].portals[portal].delayWarp,
                          icon: 1,
                          moveTo: data[row].portals[portal].moveTo,
                          posX: typeof data[row].portals[portal].posX === 'string' ? data[row].portals[portal].posX.replace('px', '') : data[row].portals[portal].posX,
                          posY: typeof data[row].portals[portal].posY === 'string' ? data[row].portals[portal].posY.replace('px', '') : data[row].portals[portal].posY,
                          stage: stage.id,
                          game: game.id,
                        });
                      }
                    }
                    // создаем порталы
                    if (values.length) {
                      const query = pgp.helpers.insert(values, columns);
                      db.none(query);
                    }

                    // создаем вопросы
                    columns = new pgp.helpers.ColumnSet([
                      'game',
                      'stage',
                      'available',
                      'icon',
                      'iconHeight',
                      'iconWidth',
                      'picture',
                      'posX',
                      'posY',
                      'question',
                      'answer',
                      'score'
                    ], {table: 'questionsTemplate'});
                    values = [];
                    for (const question in data[row].questions) {
                      if (data[row].questions.hasOwnProperty(question)) {
                        values.push({
                          game: game.id,
                          stage: stage.id,
                          available: data[row].questions[question].available,
                          icon: 1,
                          iconHeight: data[row].questions[question].iconHeight,
                          iconWidth: data[row].questions[question].iconWidth,
                          picture: data[row].questions[question].picture,
                          posX: typeof data[row].questions[question].posX === 'string' ? data[row].questions[question].posX.replace('px', '') : data[row].questions[question].posX,
                          posY: typeof data[row].questions[question].posY === 'string' ? data[row].questions[question].posY.replace('px', '') : data[row].questions[question].posY,
                          question: data[row].questions[question].question,
                          answer: data[row].questions[question].answer,
                          score: data[row].questions[question].score
                        });
                      }
                    }
                    if (values.length) {
                      const query = pgp.helpers.insert(values, columns);
                      db.none(query);
                    }

                  })
                  .catch(error => {
                    console.log('ERROR:', error);
                  });
              })
              .catch(error => {
                console.log('ERROR:', error);
              });
          }
        }

      });
    console.log('done');
  });

  socket.on('insert-stages', (data, gameCode, gameDateTime) => {
    console.log('insert-stages action');
    console.log(gameCode);
    console.log(gameDateTime);
    db.one('select id from app where "nextGameData" = $1', gameDateTime).then((app) => {
      db.one('select id from games where code = $1', gameCode)
        .then((game) => {
          for (const row in data) {
            if (data.hasOwnProperty(row)) {
              // создаем набор карт
              db.one('insert into "stageBackground" ("posX", "posY", src) values ($1, $2, $3) returning id', [
                data[row].map.posX,
                data[row].map.posY,
                data[row].map.src,
              ])
                .then(map => {
                  // создаем уровень
                  db.one('insert into "gamesStages" ("stageModule","code","title","portalIcon","questionIcon", map, app) values ($1, $2, $3, $4, $5, $6, $7) returning id', [
                    1,
                    data[row].stage,
                    data[row].title,
                    1,
                    1,
                    map.id,
                    app.id
                  ])
                    .then(stage => {
                      let columns = new pgp.helpers.ColumnSet([
                        'available',
                        'delayWarp',
                        'icon',
                        'moveTo',
                        'posX',
                        'posY',
                        'stage',
                        'game',
                        'app'
                      ], {table: 'gamesPortals'});

                      let values = [];
                      for (const portal in data[row].portals) {
                        if (data[row].portals.hasOwnProperty(portal)) {
                          values.push({
                            available: data[row].portals[portal].available,
                            delayWarp: data[row].portals[portal].delayWarp,
                            icon: 1,
                            moveTo: data[row].portals[portal].moveTo,
                            posX: typeof data[row].portals[portal].posX === 'string' ? data[row].portals[portal].posX.replace('px', '') : data[row].portals[portal].posX,
                            posY: typeof data[row].portals[portal].posY === 'string' ? data[row].portals[portal].posY.replace('px', '') : data[row].portals[portal].posY,
                            stage: stage.id,
                            game: game.id,
                            app: app.id
                          });
                        }
                      }
                      // создаем порталы
                      if (values.length) {
                        const query = pgp.helpers.insert(values, columns);
                        db.none(query);
                      }

                      // создаем вопросы
                      columns = new pgp.helpers.ColumnSet([
                        'game',
                        'stage',
                        'available',
                        'icon',
                        'iconHeight',
                        'iconWidth',
                        'picture',
                        'posX',
                        'posY',
                        'question',
                        'answer',
                        'score',
                        'app'
                      ], {table: 'gamesQuestions'});
                      values = [];
                      for (const question in data[row].questions) {
                        if (data[row].questions.hasOwnProperty(question)) {
                          values.push({
                            game: game.id,
                            stage: stage.id,
                            available: data[row].questions[question].available,
                            icon: 1,
                            iconHeight: data[row].questions[question].iconHeight,
                            iconWidth: data[row].questions[question].iconWidth,
                            picture: data[row].questions[question].picture,
                            posX: typeof data[row].questions[question].posX === 'string' ? data[row].questions[question].posX.replace('px', '') : data[row].questions[question].posX,
                            posY: typeof data[row].questions[question].posY === 'string' ? data[row].questions[question].posY.replace('px', '') : data[row].questions[question].posY,
                            question: data[row].questions[question].question,
                            answer: data[row].questions[question].answer,
                            score: data[row].questions[question].score,
                            app: app.id
                          });
                        }
                      }
                      if (values.length) {
                        const query = pgp.helpers.insert(values, columns);
                        db.none(query);
                      }

                    })
                    .catch(error => {
                      console.log('ERROR:', error);
                    });
                })
                .catch(error => {
                  console.log('ERROR:', error);
                });
            }
          }

        });

    });


    console.log('done');
  });

  socket.on('insert-app', (apps) => {
    let gamesExists = [];
    db.many('select * from games')
      .then((games) => {
        gamesExists = games;
        let columns = new pgp.helpers.ColumnSet([
          'game',
          'authModule',
          'nextGameData',
          'profileModule',
          'state',
        ], {table: 'app'});
        let values = [];

        apps.forEach((app) => {
          values.push({
            game: gamesExists.filter(game => game.code === app.activeGame)[0].id,
            authModule: 1,
            nextGameData: app.nextGameData,
            profileModule: 1,
            state: 3,
          })
        });

        if (values.length) {
            const query = pgp.helpers.insert(values, columns);
            db.none(query).then(() => {
              console.log('insert-app is done');
            });
        }
      });
  });

  socket.on("disconnect", async () => {
    console.log('user disconnected');
  });

}));

httpServer.listen(8009, () => {
  console.log('start listen on 192.168.100.4:8009');
});
