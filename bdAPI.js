const pgp = require('pg-promise')();
const app = require('express')();
// const fastify = require('fastify')({
//   logger: true
// });

const httpServer = require("http").createServer(app);
const io = require("socket.io")(httpServer, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

//const connectionString = `postgresql://qb:qb@192.168.100.4:5432/qb`;
const connectionString = `postgresql://##:########@ratatosk.studio:5432/##`;
// const connectionString = `postgresql://##:#########@localhost:5432/##`;
const db = pgp(connectionString);

io.on('connection', (socket => {
  console.log('new connect');
  let sco;

  db.connect({direct: true})
    .then(obj => {
      sco = obj; // save the connection object;
      sco.client.on('notification', data => {
        // console.log('notify from bd:', data.payload);
        const event = data.payload.split(',');
        switch (event[0]) {
          case 'app': {
            socket.emit('app-change-state', event[1], event[2])
            break;
          }
          case 'gamesQuestions': {
            const question = data.payload.substr(data.payload.indexOf(',')+1).split('#EOF#');
            socket.emit('update-game-question', {
              app: Number(question[0].substr(question[0].indexOf(':') +1)),
              id: Number(question[1].substr(question[1].indexOf(':') +1)),
              available: question[2].substr(question[2].indexOf(':') + 1) === "true",
              busy: question[3].substr(question[3].indexOf(':') +1) === "true",
              empty: question[4].substr(question[4].indexOf(':') +1) === "true",
              icon: Number(question[5].substr(question[5].indexOf(':') +1)),
              iconWidth: Number(question[6].substr(question[6].indexOf(':') +1)),
              iconHeight: Number(question[7].substr(question[7].indexOf(':') +1)),
              picture: question[8].substr(question[8].indexOf(':') +1),
              posX: Number(question[9].substr(question[9].indexOf(':') +1)),
              posY: Number(question[10].substr(question[10].indexOf(':') +1)),
              question: question[11].substr(question[11].indexOf(':') +1),
              answer: question[12].substr(question[12].indexOf(':') +1),
              score: Number(question[13].substr(question[13].indexOf(':') +1)),
            });
            break;
          }
          case 'gamesUsersData': {
            const userScore = data.payload.substr(data.payload.indexOf(',')+1).split('#EOF#');
            const userID = userScore[1].substr(userScore[1].indexOf(':') +1);
            db.one('select username from users where id = $1', [userID])
              .then((username) => {
                socket.emit('change-users-scores', {
                  refApp: userScore[0].substr(userScore[0].indexOf(':') +1),
                  refUser: userScore[1].substr(userScore[1].indexOf(':') +1),
                  score: userScore[2].substr(userScore[2].indexOf(':') +1),
                  username: username.username
                });
              });

            break;
          }
        }

      });

      // execute all the queries you need:
      return sco.none('LISTEN $1:name', 'qbchannel');
    })
    .then(data => {
      // success
      console.log('data', data);
    })
    .catch(error => {
      // error
      console.log(`error: ${error}`);
    });


  socket.on("get-gametemplate-info", (gameCode) => {
    console.log('get-gametemplate-info');
    let stages;
    let portals;
    let questions;

    db.many('select * from "stagesTemplate" where code like \'%$1#%\'', [gameCode])
      .then((result) => {
          stages = result;
          db.many('select * from "stagesTemplate" as st left join "portalsTemplate" as pt on pt.stage = st.id  where code like \'%$1#%\'', [gameCode])
            .then((result) => {
                portals = result;
                db.many('select * from "stagesTemplate" as st left join "questionsTemplate" as pt on pt.stage = st.id  where code like \'%$1#%\'', [gameCode])
                  .then((result) => {
                      questions = result;
                      db.one('select count (*) from games as g left join app as a on g.id = a.game where code = $1', [gameCode])
                        .then((count) => {
                          socket.emit('get-gametemplate-info', stages, portals, questions, Number(count.count));
                        })
                        .catch((error) => {
                          console.error('select count (*) from games', error);
                        })
                    }
                  )
                  .catch((error) => {
                    console.error('select * from "stagesTemplate" as st left join "questionsTemplate"', error);
                  })
                ;
              }
            )
            .catch((error) => {
              console.error('select * from "stagesTemplate" as st left join "portalsTemplate"', error);
            })
          ;

        }
      )
      .catch((error) => {
        console.error('select * from "stagesTemplate"', error);
      })
    ;
  })

  socket.on("get-app-games", () => {
    db.many('select a.*, g.title as title, g.code as code from app a left join games g on a.game = g.id order by "nextGameData" DESC')
      .then((apps) => {
        socket.emit("get-app-games", apps);
      });
  });

  socket.on("get-game-titles", () => {
    db.many(`select g.title, g.code as id, tt.title as type
                   from games g
                   left join "templateType" tt on g.type = tt.id
                   order by g.title`)
      .then((gamesTitles) => {
        socket.emit("get-game-titles", gamesTitles);
      });
  })

  socket.on("get-game-title", (gameID) => {
    db.one(`select g.title, tt.title
                  from app
                  left join games g on app.game = g.id
                  left join "templateType" tt on g.type = tt.id
                  where app.id = $1;`, gameID)
      .then((title) => {
        socket.emit("get-game-title", title.title);
      });
  })

  socket.on("schedule-new-game", (app) => {
    db.one('select id from games where games.code = $1', app.activeGame)
      .then((gameID) => {
        db.one('insert into app (game, "authModule", "nextGameData", "profileModule", state) values ($1, $2, $3, $4, $5) returning id', [
          gameID.id,
          app.authModule,
          app.nextGameData,
          app.profileModule,
          2
        ]).then((newApp) => {
          // Получаем все карты из шаблона игры
          db.many('select * from "stagesTemplate" where substring(code from \'\\w+\') = $1', app.activeGame)
            .then((stages) => {
              stages.forEach((stage) => {
                // создаем уровень
                db.one('insert into "gamesStages" ("stageModule","code","title","portalIcon","questionIcon", map, app) values ($1, $2, $3, $4, $5, $6, $7) returning id', [
                  stage.stageModule,
                  stage.code,
                  stage.title,
                  stage.portalIcon,
                  stage.questionIcon,
                  stage.map,
                  newApp.id
                ])
                  .then(newStageID => {
                    // получаем порталы
                    db.manyOrNone('select * from "portalsTemplate" where stage = $1', stage.id)
                      .then((portals) => {
                        if (portals !== null) {
                          let columns = new pgp.helpers.ColumnSet([
                            'available',
                            'delayWarp',
                            'icon',
                            'moveTo',
                            'posX',
                            'posY',
                            'stage',
                            'game',
                            'app'
                          ], {table: 'gamesPortals'});

                          let values = [];
                          portals.forEach((portal) => {
                            values.push({
                              available: portal.available,
                              delayWarp: portal.delayWarp,
                              icon: portal.icon,
                              moveTo: portal.moveTo,
                              posX: portal.posX,
                              posY: portal.posY,
                              stage: newStageID.id,
                              game: gameID.id,
                              app: newApp.id
                            });
                          });
                          // создаем порталы
                          if (values.length) {
                            const query = pgp.helpers.insert(values, columns);
                            db.none(query);
                          }
                        }

                        // получаем вопросы
                        db.manyOrNone('select * from "questionsTemplate" where stage = $1', stage.id)
                          .then((questions) => {
                            if (questions !== null) {
                              columns = new pgp.helpers.ColumnSet([
                                'game',
                                'stage',
                                'available',
                                'icon',
                                'iconHeight',
                                'iconWidth',
                                'picture',
                                'posX',
                                'posY',
                                'question',
                                'answer',
                                'score',
                                'app'
                              ], {table: 'gamesQuestions'});
                              values = [];
                              questions.forEach((question) => {
                                values.push({
                                  game: question.game,
                                  stage: newStageID.id,
                                  available: question.available,
                                  icon: question.icon,
                                  iconHeight: question.iconHeight,
                                  iconWidth: question.iconWidth,
                                  picture: question.picture,
                                  posX: question.posX,
                                  posY: question.posY,
                                  question: question.question,
                                  answer: question.answer,
                                  score: question.score,
                                  app: newApp.id
                                });
                              });
                              // создаем вопросы
                              if (values.length) {
                                const query = pgp.helpers.insert(values, columns);
                                db.none(query);
                              }
                            }
                          })
                          .catch((error) => {
                            console.log('error', error);
                            socket.emit("schedule-new-game", false);
                          });
                      })
                      .catch((error) => {
                        console.log('error', error);
                        socket.emit("schedule-new-game", false);
                      });
                  })
                  .catch(error => {
                    console.log('ERROR:', error);
                    socket.emit("schedule-new-game", false);
                  });
              });
              socket.emit("schedule-new-game", true);
            })
            .catch((error) => {
              console.log('error', error);
              socket.emit("schedule-new-game", false);
            });
          })
          .catch((error) => {
            console.log(error);
            socket.emit("schedule-new-game", false);
          });

      })
      .catch((error) => {
        console.log(error);
        socket.emit("schedule-new-game", false);
      });
  });

  socket.on('schedule-new-game-check-time', (time) => {
    db.oneOrNone('select id from app where "nextGameData" = $1', time)
      .then((res) => {
        socket.emit('schedule-new-game-check-time', res === null);
      })
  });

  socket.on('game-change-state', (appID, state) => {
    db.none('update app set state = $1 where id = $2', [state, appID])
      .then(() => {
        socket.emit('game-change-state', true);
      })
      .catch(() => {
        socket.emit('game-change-state', false);
      });
  })

  socket.on('settings-get-roles', () => {
    db.manyOrNone('select * from roles order by id')
      .then((res) => {
        socket.emit('settings-get-roles', res)
      })
      .catch((error) => {
        socket.emit('settings-get-roles', false, error);
      })
  });

  // добавить роль в список
  socket.on('settings-add-role', (title, code) => {
    db.one('insert into roles (title, code) values ($1, $2) returning id', [title, code])
      .then((res) => {
        socket.emit('settings-add-role', res)
      })
      .catch((error) => {
        socket.emit('settings-add-role', false, error);
      })
  });

  // установка новой роли по умолчанию
  socket.on('settings-setdefault-role', (id) => {
    db.none('update roles set isDefault = false where isdefault = true').
      then(() => {
        db.none('update roles set isdefault = true where id = $1', id)
          .then(() => {
            socket.emit('settings-setdefault-role', true);
          })
          .catch((error) => {
            socket.emit('settings-setdefault-role', false, error);
          });
      })
      .catch((error) => {
        socket.emit('settings-setdefault-role', false, error);
      });
  })

  socket.on('auth-register-email', (account) => {
    db.none('insert into users (role, username, email, avatar) values ($1, $2, $3, $4)', [account.role,  account.username, account.email, account.avatar])
      .then(() => {
        socket.emit('auth-register-email', true);
      })
      .catch((error) => {
        socket.emit('auth-register-email', false, error);
      });
  });

  socket.on('auth-getuser-info', (email) => {
    db.one('select u.id as uid, u.role, u.username, u.email, u."emailVerified", u.avatar, u."activeGame", u.group, r.id as rid, r.title as title, r.code as code, r.isdefault as isdefault  from users u left join roles r on r.id = u.role where email = $1', email)
      .then((account) => {
        socket.emit('auth-getuser-info', account);
      })
      .catch((error) => {
        socket.emit('auth-getuser-info', false, error);
      })
  })

  // получить список игр с основными количественными параметрами
  socket.on('games-short-list', () => {
    db.many(`select app.id as id,
                         app."nextGameData" as gameData,
                         games.title as gameTitle,
                         games.code as gameCode,
                         gs.title as gameState,
                         (select count (*) from "gamesPortals" where app.id = "gamesPortals".app) as countPortals,
                         (select count (*) from "gamesQuestions" where app.id = "gamesQuestions".app) as countQuestions,
                         (select count (*) from "gamesStages" where app.id = "gamesStages".app) as countStages,
                         (
                           select sB.src as mapSrc from "gamesStages" as gSt
                           left join "stageBackground" as sB on gSt.map = sB.id
                           where gSt.code like '%/0' and gSt.app = app.id
                         )
                   from app
                   left join games on app.game = games.id
                   left join "gameStates" gS on app.state = gS.id
                   order by app."nextGameData" desc;`
    )
      .then((games) => {
        socket.emit('games-short-list', games);
      })
      .catch((error) => {
        socket.emit('games-short-list', false, error);
      })
  });

  socket.on('games-frontstagemap-byid', (id) => {
    db.oneOrNone(`
                          select sB.src as map
                          from app
                          left join games on app.game = games.id
                          left join "gameStates" gS on app.state = gS.id
                          left join "gamesStages" gSt on app.id = gSt.app
                          left join "stageBackground" as sB on gSt.map = sB.id
                          where app.id = $1 and gSt.code like '%/0';`, id)
      .then((result) => {
        socket.emit('games-frontstagemap-byid', result);
      })
      .catch((error) => {
        socket.emit('games-frontstagemap-byid', false, error);
      });
  });

  // получить детальное представление об игре
  socket.on('games-detail-byid', (id) => {
    db.oneOrNone(`
                          select app.id as id,
                             app."nextGameData" as gameData,
                             games.title as gameTitle,
                             games.code as gameCode,
                             gs.title as gameState,
                             sB.src as mapSrc,
                             (select count (*) from "gamesPortals" where app.id = "gamesPortals".app) as countPortals,
                             (select count (*) from "gamesQuestions" where app.id = "gamesQuestions".app) as countQuestions,
                             (select count (*) from "gamesStages" where app.id = "gamesStages".app) as countStages
                          from app
                          left join games on app.game = games.id
                          left join "gameStates" gS on app.state = gS.id
                          left join "gamesStages" gSt on app.id = gSt.app
                          left join "stageBackground" as sB on gSt.map = sB.id
                          where app.id = $1 and gSt.code like '%/0';`, id)
      .then((result) => {
        socket.emit('games-detail-byid', result);
      })
      .catch((error) => {
        socket.emit('games-detail-byid', false, error);
      });
  });

  socket.on('games-detail-questions-short', (appID) => {
    db.many(`select DISTINCT gS.code,
                                   gS.title,
                                   (select count (*) from "gamesQuestions" where "gamesQuestions".stage = gS.id) as countQuestions
                   from "gamesQuestions"
                   left join "gamesStages" gS on gS.id = "gamesQuestions".stage
                   where "gamesQuestions".app = $1
                   order by code`, appID)
      .then((res) => {
        socket.emit('games-detail-questions-short', res);
      })
      .catch((error) => {
        socket.emit('games-detail-questions-short', false, error);
      });
  });

  socket.on('games-detail-portals-short', (appID) => {
    db.many(`select DISTINCT gS.code,
                                   gS.title,
                                   (select count (*) from "gamesPortals" where "gamesPortals".stage = gS.id) as countPortals
                   from "gamesQuestions"
                          left join "gamesStages" gS on gS.id = "gamesQuestions".stage
                   where "gamesQuestions".app = $1
                   order by code;`, appID)
      .then((res) => {
        socket.emit('games-detail-portals-short', res);
      })
      .catch((error) => {
        socket.emit('games-detail-portals-short', false, error);
      });
  });

  socket.on('games-detail-questions-full-bystage', (gameID, stageCODE) => {
    db.many(`select available, icon, "iconHeight", "iconWidth", picture, "posX", "posY", question, answer, score, "stageModule", "questionIcon"
                   from "gamesQuestions" as gQ
                   left join "gamesStages" gS on gS.id = gQ.stage
                   where gQ.app = $1 and gS.code = $2;`, [gameID, stageCODE])
      .then((questions) => {
        socket.emit('games-detail-questions-full-bystage', questions);
      })
      .catch((error) => {
        socket.emit('games-detail-questions-full-bystage', false, error);
      });
  });

  socket.on('games-detail-portals-full-bystage', (gameID, stageCODE) => {
    db.many(`select available, "delayWarp", icon, "moveTo", "posX", "posY", "stageModule"
                   from "gamesPortals" as gP
                   left join "gamesStages" gS on gS.id = gP.stage
                   where gP.app = $1 and gS.code = $2;`, [gameID, stageCODE])
      .then((portals) => {
        socket.emit('games-detail-portals-full-bystage', portals);
      })
      .catch((error) => {
        socket.emit('games-detail-portals-full-bystage', false, error);
      });
  });

  socket.on('game-template-fulldata', (gameID) => {
    db.oneOrNone(`select g.id, g.title, g.code, g.picture, tt.id as typeId, tt.title as typeTitle, tt.code as typeCode
                        from games as g
                        left join "templateType" as tt on g.type = tt.id
                        where g.code = $1`, [gameID])
      .then((result) => {
        socket.emit('game-template-fulldata', result)
      })
      .catch((error) => {
        socket.emit('game-template-fulldata', false, error);
      });
  })

  socket.on('get-template-types', () => {
    db.many('select id, title, code from "templateType"')
      .then((types) => {
        socket.emit('get-template-types', types);
      })
      .catch((error) => {
        socket.emit('get-template-types', false, error);
      })
    ;
  })

  socket.on('update-template-field', (gameCode, field, value) => {
    const query = `update games set ${field} = $1 where code = $2`;
    db.none(query, [value, gameCode])
      .then(() => {
        socket.emit('update-template-field', true);
      })
      .catch((error) => {
        socket.emit('update-template-field', false, error);
      });
  })

  socket.on('template-stage-detail-bycode', (code) => {
    const query = ` select st.id as stID,
                           sm.code as smCode,
                           sm.title as smTitle,
                           sm.description as smDescription,
                           sm.uuid as smUUID,
                           st.code as stCode,
                           st.title as stTitle,
                           pi.src as portalIcon,
                           qi.src as questionIcon,
                           sb.src as mapSrc,
                           sb."posX" as mapPosX,
                           sb."posY" as mapPosY
                    from "stagesTemplate" as st
                    left join "stageModules" as sm on st."stageModule" = sm.id
                    left join icons pi on st."portalIcon" = pi.id
                    left join icons qi on st."questionIcon" = qi.id
                    left join "stageBackground" sB on st.map = sB.id
                    where st.code LIKE '${code}%'
                    order by st.code;`
    db.manyOrNone(query)
      .then((stages) => {
        socket.emit('template-stage-detail-bycode', stages);
      })
      .catch((error) => {
        socket.emit('template-stage-detail-bycode', false, error);
      });
  })

  socket.on('activegame-stage-detail-bycode', (app, code) => {
    const query = ` select gS.id as stID,
                           sm.code as smCode,
                           sm.title as smTitle,
                           sm.description as smDescription,
                           sm.uuid as smUUID,
                           gS.code as stCode,
                           gS.title as stTitle,
                           pi.src as portalIcon,
                           qi.src as questionIcon,
                           sb.src as mapSrc,
                           sb."posX" as mapPosX,
                           sb."posY" as mapPosY
                    from "gamesStages" as gS
                    left join "stageModules" as sm on gS."stageModule" = sm.id
                    left join icons pi on gS."portalIcon" = pi.id
                    left join icons qi on gS."questionIcon" = qi.id
                    left join "stageBackground" sB on gS.map = sB.id
                    where gS.code LIKE '${code}%' and gS.app = ${app}
                    order by gS.code;`
    db.manyOrNone(query)
      .then((stages) => {
        socket.emit('activegame-stage-detail-bycode', stages);
      })
      .catch((error) => {
        socket.emit('activegame-stage-detail-bycode', false, error);
      });
  })

  socket.on('template-questions-detail-byid', (id) => {
    db.manyOrNone(`select qt.id, st.code, qt.available, qt.picture, qt.question, qt.answer, qt.score, i.src as icon, qt."iconHeight", qt."iconWidth", qt."posX", qt."posY"
                         from "questionsTemplate" qt
                         left join games g on qt.game = g.id
                         left join "stagesTemplate" st on qt.stage = st.id
                         left join icons i on qt.icon = i.id
                         where qt.game = $1
                         order by st.code`, [id])
      .then((questions) => {
        socket.emit('template-questions-detail-byid', questions);
      })
      .catch((error) => {
        socket.emit('template-questions-detail-byid', false, error);
      });
  })

  socket.on('activegame-questions-detail-byid', (id) => {
    db.manyOrNone(`select qt.id, st.code, qt.available, qt.picture, qt.question, qt.answer, qt.score, i.src as icon, qt."iconHeight", qt."iconWidth", qt."posX", qt."posY", qt.busy as busy, qt.empty as empty
                         from "gamesQuestions" qt
                         left join games g on qt.game = g.id
                         left join "gamesStages" st on qt.stage = st.id
                         left join icons i on qt.icon = i.id
                         where qt.app = $1
                         order by st.code`, [id])
      .then((questions) => {
        socket.emit('activegame-questions-detail-byid', questions);
      })
      .catch((error) => {
        socket.emit('activegame-questions-detail-byid', false, error);
      });
  })

  socket.on('template-portals-detail-byid', (id) => {
    db.manyOrNone(`select pt.id, pt.available, pt."delayWarp", i.src as icon, st.code as "moveFrom", pt."moveTo", pt."posX", pt."posY"
                         from "portalsTemplate" pt
                         left join "stagesTemplate" st on pt.stage = st.id
                         left join icons i on pt.icon = i.id
                         where pt.game = $1`, [id])
      .then((portals) => {
        socket.emit('template-portals-detail-byid', portals);
      })
      .catch((error) => {
        socket.emit('template-portals-detail-byid', false, error);
      });
  })

  socket.on('activegame-portals-detail-byid', (id) => {
    db.manyOrNone(`select pt.id, pt.available, pt."delayWarp", i.src as icon, st.code as "moveFrom", pt."moveTo", pt."posX", pt."posY"
                         from "gamesPortals" pt
                         left join "gamesStages" st on pt.stage = st.id
                         left join icons i on pt.icon = i.id
                         where pt.app = $1`, [id])
      .then((portals) => {
        socket.emit('activegame-portals-detail-byid', portals);
      })
      .catch((error) => {
        socket.emit('activegame-portals-detail-byid', false, error);
      });
  })

  socket.on('get-demo-code', (id) => {
    db.one('select id, code from games where type = 3')
      .then((gameCode) => {
        socket.emit('get-demo-code', gameCode);
      })
      .catch((error) => {
        socket.emit('get-demo-code', false, error);
      })
  })

  socket.on('drop-template-question', (id) => {
    db.none('delete from "questionsTemplate" where id = $1', [id]);
  });

  socket.on('get-active-gamecode', () => {
    db.oneOrNone('select app.id as app, code from app left join games g on app.game = g.id where state = 1')
      .then((game) => {
        socket.emit('get-active-gamecode', game);
      })
      .catch((error) => {
        socket.emit('get-active-gamecode', false, error);
      });
  });

  socket.on('get-game-userdata', (app, user) => {
    db.oneOrNone('select * from "gamesUsersData" where "refApp" = $1 and "refUser" = $2', [app, user])
      .then((gameUserData) => {
        socket.emit('get-game-userdata', gameUserData);
      })
      .catch((error) => {
        socket.emit('get-game-userdata', false, error)
      });
  });

  socket.on('update-game-userdata-stage', (app, user, refStage) => {
    db.none('update "gamesUsersData" set "refStage" = $3 where "refApp" = $1 and "refUser" = $2', [app, user, refStage]);
  });

  socket.on('update-question-busy', (question, user) => {
    db.none('update "gamesQuestions" set busy = not busy, "busyBy" = $2 where id = $1', [question, user]);
  })

  socket.on('update-question-empty', (question) => {
    db.none('update "gamesQuestions" set empty = not empty, "busyBy" = null where id = $1', [question]);
  });

  socket.on('update-user-scores', (refApp, user, score) => {
    db.none('update "gamesUsersData" set score = $1 where "refApp" = $2 and "refUser" = $3', [score, refApp, user]);
  });

  socket.on('add-user-togame', (refApp, refUser) => {
    db.one('insert into "gamesUsersData" ("refApp", "refUser", score, "refStage") values ($1, $2, 0, null) returning id', [refApp, refUser])
      .then((id) => {
        socket.emit('add-user-togame', id);
      })
      .catch((error) => {
        console.log(error);
      })
  });

  socket.on('log-try-answer', (gameID, questionID, userID, valid, answer, date, time) => {
    db.none('insert into "tryAnswers" ("refApp", "refQuestion", "refUser", success, answer, date, time) values ($1, $2, $3, $4, $5, $6, $7)', [
      gameID, questionID, userID, valid, answer, date, time
    ])
      .catch((error) => {
        console.log(error);
      });
  });

  socket.on('update-game-question', (gameID, question) => {
    db.none(
      'update "gamesQuestions" \
             set available = $3, empty = $4, busy = $5, score = $6, question = $7, answer = $8, picture = $9, icon = $10, "iconHeight" = $11, "iconWidth" = $12  \
             where app = $1 and id = $2 \
             ', [gameID, question.id,
                       question.available, question.empty, question.busy, question.score, question.question,
                       question.answer, question.picture, question.icon, question.iconHeight, question.iconWidth])
      .catch((error) => {
        console.log(error);
      });
  });

  socket.on('get-all-games-userdata', () => {
    db.manyOrNone('select * from "gamesUsersData"')
      .then((gamesUsersData) => {
        socket.emit('get-all-games-userdata', gamesUsersData);
      })
  });

  socket.on('get-game-allusersdata', (app) => {
    db.manyOrNone(`select "refApp", "refUser", "refStage", score, u.username from "gamesUsersData"
                         left join users u on "gamesUsersData"."refUser" = u.id
                         where "refApp" = $1
                         order by score desc`, [app])
      .then((gamesUsersData) => {
        socket.emit('get-game-allusersdata', gamesUsersData);
      })
  });

  socket.on('settings-get-activegame-users', () => {
    db.manyOrNone(`select gud.score, gud.id as userID, u.role, u.username, u.email, u.avatar, u."group", gs.title as gameMapTitle, gq.question
                          from "gamesUsersData" gud
                          join users u on gud."refUser" = u.id
                          join app a on gud."refApp" = a.id
                          join "gamesStages" gs on gud."refStage" = gs.id
                          left join "gamesQuestions" gq on a.id = gq.app and u.id = gq."busyBy"
                          where a.state = 1
                          order by gud.score desc
                          `, [])
      .then((activePlayers) => {
        socket.emit('settings-get-activegame-users', activePlayers)
      })
      .catch((error) => {
        console.log(error);
        socket.emit('settings-get-activegame-users', false, error);
      })
  });

  socket.on('settings-get-users', () => {
    db.manyOrNone('select * from users', [])
      .then((users) => {
        socket.emit('settings-get-users', users);
      })
      .catch((error) => {
        socket.emit('settings-get-users', false, error);
      });
  });

  socket.on('settings-update-account', (user) => {
    db.none('update users set role = $1, username = $2, email = $3, "emailVerified" = $4, avatar = $5, "activeGame" = $6, "group" = $7 where id = $8',
      [user.role, user.username, user.email, user.emailVerified, user.avatar, user.activeGame, user.group, user.id])
      .catch((error) => {
        console.log(error);
      });
  });

  socket.on('app-activegame-countdown', () => {
    db.manyOrNone('select "nextGameData" from app where state <> 3', [])
      .then((result) => {
        socket.emit('app-activegame-countdown', result);
      })
      .catch((error) => {
        socket.emit('app-activegame-countdown', false, error);
      });
  });

  socket.on('get-gameover-userdata', (app) => {
    db.manyOrNone(`
                          select u.id as userid, u.username as username, gud.score as score,
                             sum(case when taS.success = true then 1 end) as success,
                             sum(case when taS.success = false then 1 end) as failed
                          from "gamesUsersData" gud
                          join users u on gud."refUser" = u.id
                          left join "tryAnswers" taS on taS."refApp" = $1 and taS."refUser" = u.id
                          where gud."refApp" = $1
                          group by u.id, gud.score
                          order by gud.score desc;`, [app])
      .then((result) => {
        socket.emit('get-gameover-userdata', result);
      })
      .catch((error) => {
        socket.emit('get-gameover-userdata', false, error);
      });
  });

  socket.on('get-gameover-stat-questions', (app) => {
    db.one(`
                          select count(*) as total, sum(case when gq.empty = true then 1 end) as success, sum(case when gq.empty = false then 1 end) as failed
                          from "gamesQuestions" gq
                          where gq.app = $1;`, [app])
      .then((result) => {
        socket.emit('get-gameover-stat-questions', result);
      })
      .catch((error) => {
        socket.emit('get-gameover-stat-questions', false, error);
      });
  });

  socket.on('get-gameover-stat-answers', (app) => {
    db.one(`
                          select count(*) as total, sum (case when ta.success = true then 1 end) as success
                          from "tryAnswers" ta
                          where ta."refApp" = $1`, [app])
      .then((result) => {
        socket.emit('get-gameover-stat-answers', result);
      })
      .catch((error) => {
        socket.emit('get-gameover-stat-answers', false, error);
      });
  });

  socket.on("disconnect", async () => {
    console.log('user disconnected');
    sco.done(true);

  });

}));

httpServer.listen(8008, () => {
  console.log('start listen on 192.168.100.4:8008');
});

// fastify.listen(8008, (err, address) => {
//z
// })
