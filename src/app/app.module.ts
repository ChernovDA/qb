// import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/auth';
// import { USE_EMULATOR as USE_DATABASE_EMULATOR } from '@angular/fire/database';
// import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/firestore';
// import { USE_EMULATOR as USE_FUNCTIONS_EMULATOR } from '@angular/fire/functions';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AuthModule } from '@auth0/auth0-angular';
import { AppRoutingModule } from './app-routing.module';
import { ProfileModule } from './profile/profile.module';

import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { ru_RU } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import ru from '@angular/common/locales/ru';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzGridModule} from 'ng-zorro-antd/grid';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzButtonModule} from 'ng-zorro-antd/button';
import { IconDefinition } from '@ant-design/icons-angular';

registerLocaleData(ru);
import {
  UserOutline,
  LaptopOutline,
  NotificationOutline,
  MailOutline,
  AppstoreOutline,
  SettingOutline,
  PlayCircleOutline,
  DatabaseOutline,
  FundTwoTone
} from '@ant-design/icons-angular/icons';
const icons: IconDefinition[] = [
  UserOutline,
  LaptopOutline,
  NotificationOutline,
  MailOutline,
  AppstoreOutline,
  SettingOutline,
  PlayCircleOutline,
  DatabaseOutline,
  FundTwoTone
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AuthModule.forRoot({
      domain: 'ratatosk-studio.eu.auth0.com',
      clientId: 'MQuoqfSdb7yxeb87rtikAq0mwpzit3UO'
    }),
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AppRoutingModule,
    NzDividerModule,
    NzLayoutModule,
    NzMenuModule,
    NzIconModule.forRoot(icons),
    NzGridModule,
    NzEmptyModule,
    NzButtonModule,
    ProfileModule
  ],
  providers: [
    {provide: NZ_I18N, useValue: ru_RU},
    // {provide: USE_AUTH_EMULATOR, useValue: environment.useEmulators ? ['localhost', 9099] : undefined},
    // {provide: USE_DATABASE_EMULATOR, useValue: environment.useEmulators ? ['localhost', 9000] : undefined},
    // {provide: USE_FIRESTORE_EMULATOR, useValue: environment.useEmulators ? ['localhost', 8080] : undefined},
    // {provide: USE_FUNCTIONS_EMULATOR, useValue: environment.useEmulators ? ['localhost', 5001] : undefined},
  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
