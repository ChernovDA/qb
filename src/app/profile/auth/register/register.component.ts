import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {NzModalRef} from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-profile-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  constructor(private fb: FormBuilder, private modal: NzModalRef) { }
  formRegister!: FormGroup;

  static patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        // if control is empty return no error
        return null;
      }
      // test the value of the control against the regexp supplied
      const valid = regex.test(control.value);
      // if true, return no error (no error), else return error passed in the second parameter
      return valid ? null : error;
    };
  }

  ngOnInit(): void {
    this.formRegister = this.fb.group({
      // email: ['TestApi@ratatosk.studio', [Validators.required, Validators.email]],
      // username: ['TestApi1', Validators.required],
      // password: ['TestApi1', Validators.required],
      email: [null, [Validators.required, Validators.email]],
      username: [null, Validators.required],
      password: [null, [Validators.required,
        RegisterComponent.patternValidator(/\d/, {hasNumber: true}),
        RegisterComponent.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
        RegisterComponent.patternValidator(/[a-z]/, {hasSmallCase: true}),
        RegisterComponent.patternValidator(/[!@#$%^&*()_+\-=\.]/, {hasSpecialCharacters: true}),
        Validators.minLength(4),
        Validators.maxLength(14)
      ]],
    });
  }

  registerByEmail(): void {
    for (const i in this.formRegister.controls) {
      if (this.formRegister.controls.hasOwnProperty(i)) {
        this.formRegister.controls[i].markAsDirty();
        this.formRegister.controls[i].updateValueAndValidity();
      }
    }
    if (this.formRegister.valid) {
      this.modal.destroy({
        user: this.formRegister.value
      });
    }
  }


  cancel(): void {
    this.modal.destroy();
  }
}
