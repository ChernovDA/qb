import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

import { PlayGameEngineV1Component } from './play-game-engine-v1/play-game-engine-v1.component';
import { ListPlayableGamesComponent } from './list-playable-games/list-playable-games.component';
import { GameOverComponent } from './game-over/game-over.component';

import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzPipesModule } from 'ng-zorro-antd/pipes';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzFormModule } from 'ng-zorro-antd/form';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzTableModule } from 'ng-zorro-antd/table';

import {
  QuestionOutline
} from '@ant-design/icons-angular/icons';
import {IconDefinition} from '@ant-design/icons-angular';
import { QuestionModalComponent } from './play-game-engine-v1/question-modal/question-modal.component';
const icons: IconDefinition[] = [
  QuestionOutline
];


@NgModule({
  declarations: [
    PlayGameEngineV1Component,
    ListPlayableGamesComponent,
    GameOverComponent,
    QuestionModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: PlayGameEngineV1Component },
      { path: 'games-list', component: ListPlayableGamesComponent},
      { path: 'game-over', component: GameOverComponent }
    ]),
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    NzSpinModule,
    NzModalModule,
    NzButtonModule,
    NzGridModule,
    NzImageModule,
    NzPipesModule,
    NzInputModule,
    NzAlertModule,
    NzCardModule,
    NzCollapseModule,
    NzDescriptionsModule,
    NzDropDownModule,
    NzIconModule,
    NzDividerModule,
    NzDrawerModule,
    NzSwitchModule,
    NzFormModule,
    CKEditorModule,
    NzInputNumberModule,
    NzUploadModule,
    NzNotificationModule,
    NzSpaceModule,
    NzTagModule,
    NzLayoutModule,
    NzTypographyModule,
    NzAvatarModule,
    NzListModule,
    NzTimelineModule,
    NzBadgeModule,
    NzStatisticModule,
    NzResultModule,
    NzTableModule,
    NzIconModule.forChild(icons),
  ],
  exports: [
    RouterModule
  ]
})
export class PlaygroundModule { }
