import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {SocketService} from '../../socket.service';
import {ShortGamesListInterface} from '../../interfaces/short-games-list';
import {Router} from '@angular/router';
import {SocketAuthService} from '../../socket-auth.service';
import {Subscription} from 'rxjs';
import {UserAccount} from '../../interfaces/user-account';
import {SocketPlaygroundService} from '../../socket-playground.service';

@Component({
  selector: 'app-list-playable-games',
  templateUrl: './list-playable-games.component.html',
  styleUrls: ['./list-playable-games.component.less'],
  encapsulation: ViewEncapsulation.None
})

export class ListPlayableGamesComponent implements OnInit, OnDestroy {
  gamesList: {
    active: ShortGamesListInterface[],
    announce: ShortGamesListInterface[];
    next: ShortGamesListInterface[];
  };
  userProfile: UserAccount = null;
  gamesRegistered: {
    title: string,
    gamedata: string;
  }[] = [];
  tillGame?: number;
  gamers: number;
  windowWidth: number;
  timer: ReturnType<typeof setInterval>;
  timerTillGame: ReturnType<typeof setInterval>;
  allGamesListSubscription?: Subscription;
  allGamesUsersData?: Subscription;
  userInfoSubscription?: Subscription;
  appStateSubscription?: Subscription;

  constructor(public socket: SocketService, public route: Router, public socketAuth: SocketAuthService,
              private socketPlayground: SocketPlaygroundService
  ) {

    this.gamesList = {active: null, announce: null, next: null};
    this.gamers = 0;
    this.windowWidth = innerWidth;
  }

  ngOnInit(): void {
    this.socket.getAllGames();

    this.timer = setInterval(() => {
      if (this.gamesList.next !== null && this.userProfile !== null) {
        clearTimeout(this.timer);
        this.socketPlayground.getAllGamesUsersData();
        this.allGamesUsersData = this.socketPlayground.allGamesUsersData.subscribe((data) => {
          data.filter(userData => userData.refUser === this.userProfile.id).forEach((gameUserData) => {
            const gameRegistered = this.gamesList.next.find(game => game.id === gameUserData.refApp);
            if (gameRegistered !== undefined) {
              this.gamesRegistered.push({
                title: gameRegistered.gametitle,
                gamedata: gameRegistered.gamedata
              });
            }
          });

          this.gamesList.next = this.gamesList.next.map((activeGame) => {
            const isRegistered = data.find(game => game.refApp === activeGame.id && game.refUser === this.userProfile.id);
            activeGame.canPlay = isRegistered !== undefined && activeGame.gamestate === 'active';
            return activeGame;
          });
        });
      }
    }, 500);

    this.allGamesListSubscription = this.socket.allGamesShortList.subscribe((games) => {
      this.gamesList.active = games.filter(game => game.gamestate === 'active');
      this.gamesList.announce = games.filter(game => game.gamestate === 'announce' || game.gamestate === 'preparing');
      this.gamesList.next = this.gamesList.active.length ? this.gamesList.active : this.gamesList.announce;
    });

    this.timerTillGame = setInterval(() => {
      if (this.socketPlayground.playRemain !== undefined) {
        clearInterval(this.timerTillGame);
        this.tillGame = Date.now() + this.socketPlayground.playRemain - 1000 * 60 * 60;
      }
    }, 500);

    if (this.socketAuth.account === undefined) {
      this.userInfoSubscription = this.socketAuth.userInfo.subscribe((profile) => {
        this.userProfile = profile;
      });
    } else {
      this.userProfile = this.socketAuth.account;
    }

    this.appStateSubscription = this.socketPlayground.startGameStateListener().subscribe((state) => {
      this.gamesList.next[0].canPlay = false;
      if (state.preparing || state.announce) {
        this.gamesList.next[0].canPlay = false;
      } else if (state.active) {
        this.gamesList.next[0].canPlay = true;
      }
    });
  }

  ngOnDestroy(): void {
    this.userInfoSubscription?.unsubscribe();
    this.allGamesUsersData?.unsubscribe();
    this.allGamesListSubscription?.unsubscribe();
    this.appStateSubscription?.unsubscribe();
  }

  changeAvatar(): void {
    console.log('avatar change');
  }

  doGame(game: ShortGamesListInterface): void {
    if (game.canPlay) {
      this.route.navigate(['playground']);
    } else {
      if (!this.gamesRegistered.length) {
        this.socketPlayground.addUserToGame(game.id, this.userProfile.id);
        this.socketPlayground.newUserData.subscribe((userData) => {
          this.gamesRegistered.push({
            title: game.gametitle,
            gamedata: game.gamedata
          });
        });
      }
    }
  }

  enableJoinGame(): void {
    this.gamesList.next[0].canPlay = true;
  }
}
