import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {SocketPlaygroundService} from '../../socket-playground.service';
import {Subscription} from 'rxjs';
import {SocketService} from '../../socket.service';
import {GameResult, GameResultAnswers, GameResultQuestions} from '../../interfaces/game-result';

@Component({
  selector: 'app-game-over',
  templateUrl: './game-over.component.html',
  styleUrls: ['./game-over.component.less']
})
export class GameOverComponent implements OnInit, OnDestroy {
  users: GameResult[] = [];
  questions?: GameResultQuestions;
  answers?: GameResultAnswers;
  activeGame: number;

  usersSubscription?: Subscription;
  questionsSubscription?: Subscription;
  answersSubscription?: Subscription;
  appGamesListSubscription?: Subscription;

  constructor(private socketPlayground: SocketPlaygroundService, private socketService: SocketService) {
    this.socketService.getAppGames();
  }

  ngOnInit(): void {
    this.appGamesListSubscription = this.socketService.appGames.subscribe((games) => {
      const g = games.filter((game) => game.state['gameOver'] === true);
      if (g.length) {
        this.activeGame = Number(g[0].id);
      }

      this.socketPlayground.getGameOverUsersData(this.activeGame);
      this.socketPlayground.getGameOverUsersQuestions(this.activeGame);
      this.socketPlayground.getGameOverUsersAnswers(this.activeGame);

      this.usersSubscription = this.socketPlayground.gameOverUsersData.subscribe((gamers) => {
        this.users = gamers;
        this.usersSubscription.unsubscribe();
      });
      this.questionsSubscription = this.socketPlayground.gameOverUsersQuestions.subscribe((questions) => {
        this.questions = questions;
      });
      this.answersSubscription = this.socketPlayground.gameOverUsersAnswers.subscribe((answers) => {
        this.answers = answers;
      });
    });

  }

  ngOnDestroy(): void {
    this.usersSubscription?.unsubscribe();
    this.appGamesListSubscription?.unsubscribe();
    this.questionsSubscription?.unsubscribe();
    this.answersSubscription?.unsubscribe();
  }
}
