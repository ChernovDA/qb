import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TemplateQuestionInterface} from '../../../interfaces/game-template-main-info';
import {NzModalRef} from 'ng-zorro-antd/modal';
import {SocketAuthService} from '../../../socket-auth.service';
import {SocketPlaygroundService} from '../../../socket-playground.service';

@Component({
  selector: 'app-question-modal',
  templateUrl: './question-modal.component.html',
  styleUrls: ['./question-modal.component.less']
})
export class QuestionModalComponent implements OnInit, OnDestroy {
  @Input()
  currentQuestion!: TemplateQuestionInterface;
  @Input()
  appID!: number;

  questionHasPicture: boolean;
  answerCheckState: {
    checking: boolean,
    valid: boolean,
    notValid: boolean
  } = {
    checking: true,
    valid: false,
    notValid: false
  };
  userAnswer?: string;
  timerAnswerResultShow: ReturnType<typeof setTimeout>;

  constructor(private modal: NzModalRef, private socketAuth: SocketAuthService, private socketPlayground: SocketPlaygroundService) {}

  ngOnInit(): void {
    this.questionHasPicture = this.currentQuestion.picture !== '';
    this.updateBusyStatus();
  }

  ngOnDestroy(): void {
    this.updateBusyStatus();
  }

  checkAnswer(): void {
    if (this.userAnswer.length) {
      if (this.currentQuestion.answers !== undefined) {
        if (this.currentQuestion.answers.findIndex((answer) => answer.toLowerCase() === this.userAnswer.toLowerCase()) !== -1) {
          this.answerCheckState = {
            checking: false,
            notValid: false,
            valid: true
          };
          this.updateEmptyStatus();
          this.updateUserScore();
          this.logTryAnswer(true, this.userAnswer);
          this.timerAnswerResultShow = setTimeout(() => {
            this.handleCancel();
          }, 1500);

        } else {
          this.answerCheckState = {
            checking: false,
            notValid: true,
            valid: false
          };
          this.logTryAnswer(false, this.userAnswer);
          this.timerAnswerResultShow = setTimeout(() => {
            this.answerCheckState = {
              checking: true,
              notValid: false,
              valid: false
            };
          }, 1500);
        }
      }
    }
  }

  handleCancel(): void {
    this.modal.destroy();
    this.answerCheckState = {
      checking: true,
      notValid: false,
      valid: false
    };
  }

  updateEmptyStatus(): void {
    this.socketPlayground.toggleEmpty(this.currentQuestion.id);
  }

  updateBusyStatus(): void {
    this.socketPlayground.updateBusyStatus(this.currentQuestion.id, this.currentQuestion.busy ? this.socketAuth.account.id : null);
  }

  updateUserScore(): void {
    this.socketAuth.account.gameData.score += this.currentQuestion.score;
    this.socketPlayground.updateUserScores(this.appID, this.socketAuth.account.id, this.socketAuth.account.gameData.score);
  }

  logTryAnswer(valid: boolean, answer: string): void {
    this.socketPlayground.logTryAnswer(this.appID, this.currentQuestion.id, this.socketAuth.account.id, valid, answer);
  }
}
