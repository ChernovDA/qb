import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {SocketService} from '../../socket.service';
import {Subscription} from 'rxjs';
import * as Konva from 'konva';
import {TemplatePortalInterface, TemplateQuestionInterface} from '../../interfaces/game-template-main-info';
import {TemplateStageDetailInterface} from '../../interfaces/template-stage-detail';
import {ActiveGameInterface} from '../../interfaces/active-game';
import {SocketAuthService} from '../../socket-auth.service';
import {SocketPlaygroundService} from '../../socket-playground.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as InlineEditor from '@ckeditor/ckeditor5-build-inline';
import {NzMessageService} from 'ng-zorro-antd/message';
import {NzUploadChangeParam} from 'ng-zorro-antd/upload';
import {NzNotificationService} from 'ng-zorro-antd/notification';
import {Router} from '@angular/router';
import {GameUserData} from '../../interfaces/user-account';
import {NzModalService} from 'ng-zorro-antd/modal';
import {QuestionState} from '../../interfaces/question-state';
import {QuestionModalComponent} from './question-modal/question-modal.component';

@Component({
  selector: 'app-play-game-engine-v1',
  templateUrl: './play-game-engine-v1.component.html',
  styleUrls: ['./play-game-engine-v1.component.less'],
  providers: [ NzMessageService ],
  encapsulation: ViewEncapsulation.None
})
export class PlayGameEngineV1Component implements OnInit, OnDestroy {
  public editor = InlineEditor;

  gameLoading: boolean;
  questionLoaded: boolean;
  portalsLoaded: boolean;
  stagesLoaded: boolean;
  showQuestionWindow: boolean;
  showDrawerEditQuestion: boolean;
  showDrawerScoreWnd: boolean;
  userAnswer: string;
  timerAnswerResultShow: ReturnType<typeof setTimeout>;
  timer: ReturnType<typeof setInterval>;
  timerMouseEvent: any;
  stage: any;
  private width: number;
  private height: number;
  playgroundAdditionalRights: {
    viewMapStat: boolean,
    viewGameStat: boolean,
    editQuestion: boolean,
    editModeEnabled: boolean
  };

  gameCode: ActiveGameInterface;
  questionHasPicture: boolean;
  currentQuestion: TemplateQuestionInterface = null;
  devWindowCurrentQuestion: TemplateQuestionInterface = null;
  gameStages: TemplateStageDetailInterface[] = [];
  gameQuestions: TemplateQuestionInterface[] = [];
  gamePortals: TemplatePortalInterface[] = [];
  userStatistics: GameUserData[] = [];

  currentStage: TemplateStageDetailInterface = null;
  currentStageQuestions: TemplateQuestionInterface[] = null;
  currentStageQuestionsStatus: {
    remain: number;
    empty: number;
    busy: number;
  };
  currentStagePortals: TemplatePortalInterface[] = null;

  gameCodeSubscription?: Subscription;
  gameStagesSubscription?: Subscription;
  gameQuestionSubscription?: Subscription;
  gamePortalSubscription?: Subscription;
  userInfoSubscription?: Subscription;
  gameUserDataSubscription?: Subscription;
  listenerQuestionUpdates?: Subscription;
  listenerChangeScore?: Subscription;

  questionForm!: FormGroup;

  countdown: number;
  modalInfoMapLoading: boolean;

  constructor(
    private fb: FormBuilder, private notification: NzNotificationService,
    public socket: SocketService, public socketAuth: SocketAuthService, public socketPlayground: SocketPlaygroundService,
    public route: Router,
    public modal: NzModalService
  ) {
    // this.countdown = Date.now() + 1000 * 60 * 60 * 24;
    this.countdown = 1000;
    const countdownSubscribe = this.socketPlayground.playGameCountdown().subscribe((countdown) => {
      if (countdown.over > 0) {
        this.countdown = countdown.over;
      }
      countdownSubscribe.unsubscribe();
    });

    this.gameLoading = true;
    this.modalInfoMapLoading = false;
    this.width = window.innerWidth;
    this.height = window.innerHeight;

    this.showQuestionWindow = false;
    this.showDrawerEditQuestion = false;
    this.showDrawerScoreWnd = false;
    this.stagesLoaded = false;
    this.questionLoaded = false;
    this.portalsLoaded = false;
    this.userAnswer = '';
    this.playgroundAdditionalRights = { viewMapStat: false, viewGameStat: false, editQuestion: false, editModeEnabled: true};

    this.socket.getCurrentActiveGameCode();
  }

  ngOnInit(): void {
    this.socketPlayground.startGameStateListener().subscribe((state) => {
      if (state.preparing) {
        this.route.navigate(['/playground', 'games-list']);
      } else if (state.gameOver) {
        this.route.navigate(['/playground', 'game-over']);
      }
    });

    this.listenerQuestionUpdates = this.socketPlayground.listenUpdateQuestionData.subscribe((question) => {
      const index = this.gameQuestions.findIndex((q) => q.id === question.id);
      this.gameQuestions[index] = question;
      if (this.devWindowCurrentQuestion !== null) {
        this.devWindowCurrentQuestion = question;
      }
    });

    this.questionForm = this.fb.group({
      id: [null],
      available: [null],
      empty: [null],
      busy: [null],
      score: [null],
      question: [null, Validators.required],
      answer: [null, Validators.required],
      picture: [null],
      icon: [null],
      iconHeight: [null],
      iconWidth: [null],
    });

    this.timer = setInterval(() => {
      if (this.stagesLoaded && this.questionLoaded && this.portalsLoaded) {
        this.gameLoading = false;
        switch (this.gameStages.find((stage) => stage.stageCode.indexOf('/0')).stageModule.uuid) {
          case 'QIG1b6TK0cCPJ3UwMItA': {
            if (this.socketAuth.account.gameData && this.socketAuth.account.gameData.refStage !== null) {
              this.currentStage = this.gameStages.find((stage) => stage.stageID === this.socketAuth.account.gameData.refStage);
              this.currentStageQuestions = this.gameQuestions.filter((q) => q.code === this.currentStage.stageCode);
              this.currentStagePortals = this.gamePortals.filter((q) => q.moveFrom === this.currentStage.stageCode);
            } else {
              this.currentStage = this.gameStages.find((stage) => stage.stageCode.indexOf('/0'));
              this.currentStageQuestions = this.gameQuestions.filter((q) => q.code.indexOf('/0') !== -1);
              this.currentStagePortals = this.gamePortals.filter((q) => q.moveFrom.indexOf('/0') !== -1);
            }
            this.currentStageQuestionsStatus = {
              remain: this.currentStageQuestions.filter((q) => !q.empty && !q.busy).length,
              busy: this.currentStageQuestions.filter((q) => q.busy).length,
              empty: this.currentStageQuestions.filter((q) => q.empty).length,
            };

            this.initStage_v1(this.currentStage, this.currentStageQuestions, this.currentStagePortals);
            this.playgroundAdditionalRights.viewMapStat = this.socketAuth.account && this.socketAuth.account.role === 1;
            this.playgroundAdditionalRights.editQuestion = this.socketAuth.account && this.socketAuth.account.role === 1;

            clearTimeout(this.timer);
            break;
          }
        }
      } else {
        console.log(`not loaded: `, this.stagesLoaded, this.questionLoaded, this.portalsLoaded);
      }
    }, 1000);

    this.gameCodeSubscription = this.socket.currentActiveGameCode.subscribe((gameCode) => {
      this.gameCode = gameCode;
      this.checkUserRegFields();
      this.socketPlayground.getGameAllUsersData(this.gameCode.app);

      this.socket.getGameStageDetailsByCode(this.gameCode.app, this.gameCode.code);
      this.gameStagesSubscription = this.socket.gameStageDetailsByCode.subscribe((stages) => {
        this.gameStages = stages.map((stage) => {
          stage.map.src = `${window.location.origin}${stage.map.src}`;
          stage.questionIcon = `${window.location.origin}${stage.questionIcon}`;
          stage.portalIcon = `${window.location.origin}${stage.portalIcon}`;
          return stage;
        });

        this.stagesLoaded = true;

        this.socket.getActiveGameQuestions(this.gameCode.app);
        this.socket.getActiveGamePortals(this.gameCode.app);

        this.gameQuestionSubscription = this.socket.activeGameQuestions.subscribe((questions) => {
          this.gameQuestions = questions.filter((question) => question.available).map((question) => {
            if (question.picture === null || question.picture === undefined) {
              question.picture = '';
            }
            if (question.answer.indexOf('/')) {
              question.answers = question.answer.split('/');
            } else if (question.answer.indexOf('|')) {
              question.answers = question.answer.split('|');
            } else{
              question.answers = Array.from(question.answer);
            }
            return question;
          });
          this.questionLoaded = true;

          this.gameCodeSubscription?.unsubscribe();
        });

        this.gamePortalSubscription = this.socket.activeGamePortals.subscribe((portals) => {
          this.gamePortals = portals.filter((portal) => portal.available);
          this.portalsLoaded = true;
          this.gamePortalSubscription?.unsubscribe();
        });
      });

      this.userInfoSubscription = this.socketPlayground.gameAllUsersData.subscribe((usersStatistics) => {
        this.userStatistics = usersStatistics;
      });

      this.listenerQuestionUpdates = this.socketPlayground.listenScoreChanges.subscribe((score) => {
        const user = this.userStatistics.find(userData => Number(userData.refUser) === Number(score.refUser) &&
                                                          Number(userData.refApp) === Number(score.refApp)
                                             );
        if (user !== undefined) {
          user.score = score.score;
        } else {
          this.userStatistics.push({
            refApp: score.refApp,
            refUser: score.refUser,
            score: score.score,
            username: score.username,
            id: score.id,
            refStage: score.refStage
          });
        }
        this.userStatistics.sort((a, b) => {
          if (Number(a.score) < Number(b.score)) {
            return 1;
          } else if (Number(a.score) > Number(b.score)) {
            return -1;
          }
          return 0;
        });
      });
    });
  }

  ngOnDestroy(): void {
      this.gameCodeSubscription?.unsubscribe();
      this.gameCodeSubscription?.unsubscribe();
      this.gameQuestionSubscription?.unsubscribe();
      this.gamePortalSubscription?.unsubscribe();
      this.userInfoSubscription?.unsubscribe();
      this.gameUserDataSubscription?.unsubscribe();
      this.listenerQuestionUpdates?.unsubscribe();
      this.listenerChangeScore?.unsubscribe();
  }

  public onResize(event: Event): void {
    this.stage.size({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  public initStage_v1(currentStage: TemplateStageDetailInterface, questions: TemplateQuestionInterface[], portals: TemplatePortalInterface[]): void {
    const that = this;
    const imageObj = new Image();
    imageObj.onload = () => {
      that.modalInfoMapLoading = false;
      drawImage(imageObj);
    };
    imageObj.src = currentStage.map.src;

    function drawImage(image): void {

      that.socketAuth.account.gameData.refStage = currentStage.stageID;
      that.socketPlayground.updateUserStage(that.gameCode.app, that.socketAuth.account.id, that.socketAuth.account.gameData.refStage);
      that.stage = new Konva.default.Stage({
        container: 'playground-container',
        width: that.width,
        height: that.height,
      });

      const mapStage = new Konva.default.Image({
        image: imageObj,
      });
      // mapStage.setPosition({
      //   x: window.innerWidth / 2 - mapStage.getWidth() / 2,
      //   y: window.innerHeight / 2 - mapStage.getHeight() / 2
      // });

      const layer = new Konva.default.Layer();

      const group = new Konva.default.Group({
        draggable: true,
        x: window.innerWidth / 2 - mapStage.getWidth() / 2,
        y: window.innerHeight / 2 - mapStage.getHeight() / 2
        // x: currentStage.map.posX + window.innerWidth / 2 - mapStage.getWidth() / 2,
        // y: currentStage.map.posY + window.innerHeight / 2 - mapStage.getHeight() / 2
      });
      group.on('mouseover', () => {
        document.body.style.cursor = 'pointer';
      });
      group.on('mouseout', () => {
        document.body.style.cursor = 'default';
      });
      group.on('mousedown', () => {
        document.body.style.cursor = 'move';
      });
      group.on('mouseup', () => {
        document.body.style.cursor = 'pointer';
      });

      const questionImg = new Image();
      questionImg.src = currentStage.questionIcon;
      // TODO: icon sizes
      questionImg.width = 64;
      questionImg.height = 64;
      questionImg.onload = () => {
        questions.forEach((question) => {
          const questionIcon = new Konva.default.Image({
            x: question.posX,
            y: question.posY,
            image: questionImg,
            draggable: false,
            id: `q_${question.id}`
          });
          questionIcon.on('click', () => {
            const q = that.gameQuestions.find((gQ) => gQ.id === question.id);
            if (q.answer.indexOf('/')) {
              q.answers = question.answer.split('/');
            } else if (q.answer.indexOf('|')) {
              q.answers = q.answer.split('|');
            } else{
              q.answers = Array.from(question.answer);
            }
            if (!that.playgroundAdditionalRights.editModeEnabled) {
              that.questionHasPicture = q.picture !== '';
              that.currentQuestion = q;
              that.devWindowCurrentQuestion = q;
              // that.showQuestionWindow = true;
              // that.updateBusyStatus();
              that.createModalQuestion();
            } else {
              if (that.playgroundAdditionalRights.editQuestion) {
                that.currentQuestion = q;
                that.questionForm.patchValue({
                  id: q.id,
                  answer: q.answer,
                  question: q.question,
                  score: q.score,
                  available: q.available,
                  empty: q.empty,
                  busy: q.busy,
                  icon: q.icon,
                  iconHeight: q.iconHeight,
                  iconWidth: q.iconWidth,
                  picture: q.picture
                });
                that.showDrawerEditQuestion = true;
                that.showQuestionWindow = false;
                that.questionHasPicture = q.picture !== '';
                that.devWindowCurrentQuestion = q;
              }
            }
          });
          questionIcon.on('touchend', () => {
            const q = that.gameQuestions.find((gQ) => gQ.id === question.id)
            if (q.answer.indexOf('/')) {
              q.answers = question.answer.split('/');
            } else if (q.answer.indexOf('|')) {
              q.answers = q.answer.split('|');
            } else{
              q.answers = Array.from(question.answer);
            }
            that.questionHasPicture = q.picture !== '';
            that.currentQuestion = q;
            that.devWindowCurrentQuestion = q;
            // that.showQuestionWindow = true;
            // that.updateBusyStatus();
            that.createModalQuestion();
          });
          questionIcon.on('mouseenter', () => {
            const q = that.gameQuestions.find((gQ) => gQ.id === question.id);
            that.devWindowCurrentQuestion = q;
          });
          questionIcon.on('mouseleave', () => {
            if (!that.showQuestionWindow) {
              that.devWindowCurrentQuestion = null;
            }
          });

          group.add(questionIcon);
        });
      };

      const portalImg = new Image();
      portalImg.src = currentStage.portalIcon;
      // TODO: icon sizes
      portalImg.width = 64;
      portalImg.height = 64;
      portalImg.onload = () => {
        portals.forEach((portal) => {
          const portalIcon = new Konva.default.Image({
            x: portal.posX,
            y: portal.posY,
            image: portalImg,
            draggable: false,
            id: `p_${portal.id}`
          });
          portalIcon.on('mousedown', () => {
            currentStage = that.gameStages.find((stage) => stage.stageCode === portal.moveTo.replace('_', '/'));
            if (currentStage !== undefined) {
              questions = that.gameQuestions.filter((q) => q.code === portal.moveTo.replace('_', '/'));
              portals = that.gamePortals.filter((p) => p.moveFrom === portal.moveTo.replace('_', '/'));

              that.currentStage = currentStage;
              that.currentStageQuestions = questions;
              that.currentStagePortals = portals;
              that.currentStageQuestionsStatus = {
                remain: that.currentStageQuestions.filter((q) => !q.empty && !q.busy).length,
                busy: that.currentStageQuestions.filter((q) => q.busy).length,
                empty: that.currentStageQuestions.filter((q) => q.empty).length,
              };
              that.modalInfoMapLoading = true;
              imageObj.src = currentStage.map.src;
            }
          });
          portalIcon.on('touchend', () => {
            currentStage = that.gameStages.find((stage) => stage.stageCode === portal.moveTo.replace('_', '/'));
            if (currentStage !== undefined) {
              questions = that.gameQuestions.filter((q) => q.code === portal.moveTo.replace('_', '/'));
              portals = that.gamePortals.filter((p) => p.moveFrom === portal.moveTo.replace('_', '/'));

              that.currentStage = currentStage;
              that.currentStageQuestions = questions;
              that.currentStagePortals = portals;
              that.currentStageQuestionsStatus = {
                remain: that.currentStageQuestions.filter((q) => !q.empty && !q.busy).length,
                busy: that.currentStageQuestions.filter((q) => q.busy).length,
                empty: that.currentStageQuestions.filter((q) => q.empty).length,
              };
              that.modalInfoMapLoading = true;
              imageObj.src = currentStage.map.src;
            }
          });
          group.add(portalIcon);
        });
      };


      group.add(mapStage);

      layer.add(group);
      that.stage.add(layer);
      that.gameLoading = false;
    }
  }

  makeDeveloperWindow(): void {
    // this.socketPlayground.getGameUserData(this.gameCode.app, this.socketAuth.account.id);
    // this.socketPlayground.gameUserData.subscribe((gameUserData) => {
    //   this.socketAuth.account.gameData = gameUserData;
    //   console.log(this.socketAuth.account);
    // });
  }

  openEditWnd(): void {
    this.showDrawerEditQuestion = true;
  }

  closeEditWnd(): void {
    this.showDrawerEditQuestion = false;
  }

  switchEditMode(state: boolean): void {
    this.playgroundAdditionalRights.editModeEnabled = state;
  }

  handleChange({ file, fileList }: NzUploadChangeParam ): void {

  }

  checkUserRegFields(): void {
    this.playgroundAdditionalRights.editModeEnabled = this.socketAuth.account.role !== 4;

    this.socketPlayground.getGameUserData(this.gameCode.app, this.socketAuth.account.id);
    if (this.gameUserDataSubscription !== undefined) {
      this.gameUserDataSubscription.unsubscribe();
    }

    this.gameUserDataSubscription = this.socketPlayground.gameUserData.subscribe((gameUserData) => {
      if (gameUserData !== null) {
        this.socketAuth.account.gameData = gameUserData;
      } else {
        this.socketPlayground.addUserToGame(this.gameCode.app, this.socketAuth.account.id);
        this.socketPlayground.newUserData.subscribe((id) => {
          this.socketAuth.account.gameData = {
            score: 0,
            refStage: null,
            refApp: this.gameCode.app,
            refUser: this.socketAuth.account.id,
            id
          };
        });
      }
    });
  }

  saveChanges(): void {
    this.socketPlayground.updateQuestion(this.gameCode.app, this.questionForm.value);
    this.notification.create(
      'success',
      'Сохранение изменений в вопросе',
      'Изменения успешно импортированны в БД'
    );
  }

  cancelChanges(): void{
    this.showDrawerEditQuestion = true;
  }

  closeScoreWnd(): void {
    this.showDrawerScoreWnd = false;
  }

  emitStopGame(): void {
    // this.socketPlayground.stopGame(this.gameCode.app);
  }

  createModalForbiddenStatus(type: QuestionState): void {
    let title;
    let content;

    switch (type){
      case QuestionState.BUSY: {
        title = 'Вопрос занят кем-то другим';
        content = 'Попробуй-те данный вопрос <b>чуточку позднее</b><br>Возможно на него так и не ответят...';
        break;
      }
      case QuestionState.EMPTY: {
        title = 'Вопрос уже отвечен';
        content = 'Вами или кем-то другим, данный вопрос УЖЕ был взят';
        break;
      }
    }

    this.modal.info({
      nzTitle: title,
      nzContent: content,
      nzCentered: true,
      nzOkText: 'Закрыть'
    });
  }

  createModalQuestion(): void {
    if (this.currentQuestion.busy || this.currentQuestion.empty) {
      if (this.currentQuestion.busy) {
        this.createModalForbiddenStatus(QuestionState.BUSY);
      } else if (this.currentQuestion.empty) {
        this.createModalForbiddenStatus(QuestionState.EMPTY);
      }
    }else {
      this.modal.create({
        nzTitle: '',
        nzContent: QuestionModalComponent,
        nzComponentParams: {
          currentQuestion: this.currentQuestion,
          appID: this.gameCode.app
        },
        nzCentered: true,
        nzClosable: true
      });

      this.modal.afterAllClose.subscribe((result) => {
      });
    }
  }
}
