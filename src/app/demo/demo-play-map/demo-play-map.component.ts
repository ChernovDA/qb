import {Component, OnDestroy, OnInit} from '@angular/core';
import {SocketService} from '../../socket.service';
import {Subscription} from 'rxjs';
import * as Konva from 'konva';

import {TemplateStageDetailInterface} from '../../interfaces/template-stage-detail';
import {TemplatePortalInterface, TemplateQuestionInterface} from '../../interfaces/game-template-main-info';

@Component({
  selector: 'app-demo-play-map',
  templateUrl: './demo-play-map.component.html',
  styleUrls: ['./demo-play-map.component.less']
})
export class DemoPlayMapComponent implements OnInit, OnDestroy {
  demoLoading: boolean;
  questionLoaded: boolean;
  portalsLoaded: boolean;
  stagesLoaded: boolean;
  showQuestionWindow: boolean;
  userAnswer: string;
  timerAnswerResultShow: ReturnType<typeof setTimeout>;

  answerCheckState: {
    checking: boolean,
    valid: boolean,
    notValid: boolean
  } = {
    checking: true,
    valid: false,
    notValid: false
  };

  timer: any;

  demoCode: {id: number, code: string};

  stage: any;
  private width: number;
  private height: number;

  demoCodeSubscription: Subscription;
  templateStagesSubscription: Subscription;
  templateQuestionSubscription: Subscription;
  templatePortalSubscription: Subscription;

  demoStages: TemplateStageDetailInterface[] = [];
  demoQuestions: TemplateQuestionInterface[] = [];
  demoPortals: TemplatePortalInterface[] = [];

  questionHasPicture: boolean;
  currentQuestion: TemplateQuestionInterface = null;

  constructor(public socket: SocketService) {
    this.demoLoading = true;
    this.width = window.innerWidth;
    this.height = window.innerHeight;

    this.showQuestionWindow = false;
    this.stagesLoaded = false;
    this.questionLoaded = false;
    this.portalsLoaded = false;
    this.userAnswer = '';


    this.socket.getDemoCode();

    this.timer = setInterval(() => {
      if (this.stagesLoaded && this.questionLoaded && this.portalsLoaded) {
        clearInterval(this.timer);

        switch (this.demoStages.find((stage) => stage.stageCode.indexOf('/0')).stageModule.uuid) {
          case 'QIG1b6TK0cCPJ3UwMItA': {
            this.initStage_v1(
              this.demoStages.find((stage) => stage.stageCode.indexOf('/0')),
              this.demoQuestions.filter((q) => q.code.indexOf('/0') !== -1),
              this.demoPortals.filter((q) => q.moveFrom.indexOf('/0') !== -1)
            );
            break;
          }
        }
      } else {
        console.log('not loaded');
      }
    }, 500);
  }

  ngOnInit(): void {
    this.demoCodeSubscription = this.socket.demoCode.subscribe((gameCode) => {
      this.demoCode = gameCode;

      this.socket.getTemplateStageDetailsByCode(this.demoCode.code);
      this.templateStagesSubscription = this.socket.templateStageDetailsByCode.subscribe((stages) => {
        this.demoStages = stages.map((stage) => {
          stage.map.src = `${window.location.origin}${stage.map.src}`;
          stage.questionIcon = `${window.location.origin}${stage.questionIcon}`;
          stage.portalIcon = `${window.location.origin}${stage.portalIcon}`;
          return stage;
        });
        this.stagesLoaded = true;

        this.socket.getTemplateQuestions(this.demoCode.id);
        this.socket.getTemplatePortals(this.demoCode.id);

        this.templateQuestionSubscription = this.socket.templateQuestions.subscribe((questions) => {
          this.demoQuestions = questions.filter((question) => question.available).map((question) => {
              if (question.picture === null || question.picture === undefined) {
                question.picture = '';
              }
              if (question.answer.indexOf('/')) {
                question.answers = question.answer.split('/');
              } else if (question.answer.indexOf('|')) {
                question.answers = question.answer.split('|');
              } else{
                question.answers = Array.from(question.answer);
              }

              return question;
          });
          this.questionLoaded = true;
        });

        this.templatePortalSubscription = this.socket.templatePortals.subscribe((portals) => {
          this.demoPortals = portals.filter((portal) => portal.available);
          this.portalsLoaded = true;
        });


      });
    });
  }

  ngOnDestroy(): void {
    if (this.demoCodeSubscription !== undefined) {
      this.demoCodeSubscription.unsubscribe();
    }
    if (this.templateStagesSubscription !== undefined) {
      this.demoCodeSubscription.unsubscribe();
    }
    if (this.templateQuestionSubscription !== undefined) {
      this.templateQuestionSubscription.unsubscribe();
    }
    if (this.templatePortalSubscription !== undefined) {
      this.templatePortalSubscription.unsubscribe();
    }
  }

  public onResize(event: Event): void {
    this.stage.size({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  public initStage_v1(currentStage: TemplateStageDetailInterface, questions: TemplateQuestionInterface[], portals: TemplatePortalInterface[]): void {
    const that = this;
    const imageObj = new Image();
    imageObj.onload = () => {
      drawImage(imageObj);
    };
    imageObj.src = currentStage.map.src;

    function drawImage(image): void {

      that.stage = new Konva.default.Stage({
        container: 'demo-container',
        width: that.width,
        height: that.height
      });

      const mapStage = new Konva.default.Image({
        image: imageObj
      });

      const layer = new Konva.default.Layer();

      const group = new Konva.default.Group({
        draggable: true,
        x: currentStage.map.posX,
        y: currentStage.map.posY
      });
      group.on('mouseover', () => {
        document.body.style.cursor = 'pointer';
      });
      group.on('mouseout', () => {
        document.body.style.cursor = 'default';
      });
      group.on('mousedown', () => {
        document.body.style.cursor = 'move';
      });
      group.on('mouseup', () => {
        document.body.style.cursor = 'pointer';
      });

      const questionImg = new Image();
      questionImg.src = currentStage.questionIcon;
      // TODO: icon sizes
      questionImg.width = 64;
      questionImg.height = 64;
      questionImg.onload = () => {
        questions.forEach((question) => {
          const questionIcon = new Konva.default.Image({
            x: question.posX,
            y: question.posY,
            image: questionImg,
            draggable: false,
            id: `q_${question.id}`
          });
          questionIcon.on('mousedown', () => {
            console.log(question);
            that.questionHasPicture = question.picture !== '';
            that.currentQuestion = question;
            that.showQuestionWindow = true;
          });
          questionIcon.on('touchend', () => {
            console.log(question);
            that.questionHasPicture = question.picture !== '';
            that.currentQuestion = question;
            that.showQuestionWindow = true;
          });

          group.add(questionIcon);
        });
      };

      const portalImg = new Image();
      portalImg.src = currentStage.portalIcon;
      // TODO: icon sizes
      portalImg.width = 64;
      portalImg.height = 64;
      portalImg.onload = () => {
        portals.forEach((portal) => {
          const portalIcon = new Konva.default.Image({
            x: portal.posX,
            y: portal.posY,
            image: portalImg,
            draggable: false,
            id: `p_${portal.id}`
          });
          portalIcon.on('mousedown', () => {
            currentStage = that.demoStages.find((stage) => stage.stageCode === portal.moveTo.replace('_', '/'));
            if (currentStage !== undefined) {
              questions = that.demoQuestions.filter((q) => q.code === portal.moveTo.replace('_', '/'));
              portals = that.demoPortals.filter((p) => p.moveFrom === portal.moveTo.replace('_', '/'));

              imageObj.src = currentStage.map.src;
            }
          });
          portalIcon.on('touchend', () => {
            currentStage = that.demoStages.find((stage) => stage.stageCode === portal.moveTo.replace('_', '/'));
            if (currentStage !== undefined) {
              questions = that.demoQuestions.filter((q) => q.code === portal.moveTo.replace('_', '/'));
              portals = that.demoPortals.filter((p) => p.moveFrom === portal.moveTo.replace('_', '/'));

              imageObj.src = currentStage.map.src;
            }
          });
          group.add(portalIcon);
        });
      };


      group.add(mapStage);

      layer.add(group);
      that.stage.add(layer);

      that.demoLoading = false;
    }
  }

  handleCancel(): void {
    this.showQuestionWindow = false;
    this.answerCheckState = {
      checking: true,
      notValid: false,
      valid: false
    };
    this.userAnswer = '';
  }

  checkAnswer(): void {
    if (this.userAnswer.length) {
      if (this.currentQuestion.answers !== undefined) {
        if (this.currentQuestion.answers.findIndex((answer) => answer === this.userAnswer) !== -1) {
          this.answerCheckState = {
            checking: false,
            notValid: false,
            valid: true
          };
          this.timerAnswerResultShow = setTimeout(() => {
            this.handleCancel();
          }, 1500);

        } else {
          this.answerCheckState = {
            checking: false,
            notValid: true,
            valid: false
          };
          this.timerAnswerResultShow = setTimeout(() => {
            this.answerCheckState = {
              checking: true,
              notValid: false,
              valid: false
            };
          }, 1500);
        }
      }
    }
  }
}
