import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzPipesModule } from 'ng-zorro-antd/pipes';
import { NzAlertModule } from 'ng-zorro-antd/alert';

import { DemoPlayMapComponent } from './demo-play-map/demo-play-map.component';


@NgModule({
  declarations: [
    DemoPlayMapComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: DemoPlayMapComponent}
    ]),
    FormsModule,
    NzSpinModule,
    NzModalModule,
    NzButtonModule,
    NzGridModule,
    NzImageModule,
    NzPipesModule,
    NzInputModule,
    NzAlertModule
  ],
  exports: [
    RouterModule
  ]
})

export class DemoModule { }
