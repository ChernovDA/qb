import { Pipe, PipeTransform } from '@angular/core';

interface GameState {
  code: string;
  ru: string;
}

@Pipe({
  name: 'gameStateTranslate'
})
export class GameStatePipe implements PipeTransform {
  gamesStates: GameState[] = [
    {code: 'announce', ru: 'Анонс'},
    {code: 'gameOver', ru: 'Игра завершена'},
    {code: 'preparing', ru: 'Подготовка к игре'},
    {code: 'active', ru: 'В игре'}
  ];


  transform(value: string): string {
    return this.gamesStates.filter((state) => state.code === value)[0].ru;
  }

}
