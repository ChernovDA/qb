import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameStatePipe } from './pipes/game-state.pipe';
import {SafeHtmlPipe} from './pipes/safe-html.pipe';

@NgModule({
  declarations: [
    GameStatePipe,
    SafeHtmlPipe
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    GameStatePipe,
    SafeHtmlPipe
  ]
})
export class SharedModule { }
