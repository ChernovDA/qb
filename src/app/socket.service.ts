import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { io, Socket } from 'socket.io-client';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs';
import {GameTemplate} from './interfaces/game-template';
import {AppGame} from './interfaces/app-game';
import {GameInfo} from './interfaces/game-info';
import {ShortGamesListInterface} from './interfaces/short-games-list';
import {GameDetailsInterface} from './interfaces/game-details';
import {GameDetailsShortPortals, GameDetailsShortQuestions} from './interfaces/game-detail-short-counts';
import {GameDetailsPortalsFull, GameDetailsQuestionsFull} from './interfaces/game-detail-fullinfo';
import {GameTemplateMainInfoInterface, TemplatePortalInterface, TemplateQuestionInterface} from './interfaces/game-template-main-info';
import {GameType} from './interfaces/game-type';
import {TemplateStageDetailInterface} from './interfaces/template-stage-detail';
import {ActiveGameInterface} from './interfaces/active-game';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  socket: Socket;
  socketPG: Socket;

  constructor(private httpClient: HttpClient) {
    this.socket = io(`${environment.socketScheme}://${environment.socketHost}:${environment.socketPort}`);
    // this.socketPG = io(`${environment.socketScheme}://${environment.socketHost}:${environment.socketPortPG}`);

    this.onResponse().subscribe((msg) => {
      console.log(msg);
    });
  }

  onResponse(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('response', msg => {
        observer.next(msg);
      });
    });
  }
  // PostgreSQL import/export
  // used on /admin/dashboard/bd/import
  // Импорт таблицы из Firestore и передача в PostgreSQL
  insertToTable(data: any, gameCode: string): void {
    this.socketPG.emit('insert-stages-template', 'stagesTemplate', data, gameCode);
  }
  insertAppToTable(data: any): void {
    this.socketPG.emit('insert-app', data);
  }
  insertPastGame(data: any, gameCode: string, gameDateTime: string): void {
    this.socketPG.emit('insert-stages', data, gameCode, gameDateTime);
  }
  // *****************************************

  getDataFromTemplateGame(gameCode: string): void {
    if (gameCode.length) {
      this.socket.emit('get-gametemplate-info', gameCode);
    }
  }
  get dataFromTemplateGame(): Observable<GameTemplate[]> {
    return new Observable<GameTemplate[]>(observer => {
      this.socket.on('get-gametemplate-info', (stages, portals, questions, count) => {
        stages.map(stage => {
          stage.portals = portals.filter(portal => portal.code === stage.code);
          stage.questions = questions.filter(question => question.code === stage.code);
          stage.makedGames = count;
        });
        observer.next(stages);
      });
    });
  }

  getAppGames(): void {
    this.socket.emit('get-app-games');
  }
  get appGames(): Observable<AppGame[]> {
    return new Observable<AppGame[]>(observer => {
      this.socket.on('get-app-games', (apps) => {
        let state: number;
        apps = apps.map((app) => {
          if (typeof app.state === 'number') {
            state = app.state;
            app.state = {
              active: state === 1,
              announce: state === 2,
              gameOver: state === 3,
              preparing: state === 4,
            };
          }
          return app;
        });
        observer.next(apps);
      });
    });
  }

  getGamesTitles(): void {
    this.socket.emit('get-game-titles');
  }
  get gamesTitles(): Observable<GameInfo[]> {
    return new Observable<GameInfo[]>(observer => {
      this.socket.on('get-game-titles', (gamesTitles) => {
        observer.next(gamesTitles);
      });
    });
  }

  getGameTitle(gameID: number): void {
    this.socket.emit('get-game-title', gameID);
  }
  get gameTitle(): Observable<string> {
    return new Observable<string>((observer) => {
      this.socket.on('get-game-title', (title) => {
        observer.next(title);
      });
    });
  }

  scheduleNewGame(app: AppGame): void {
    this.socket.emit('schedule-new-game', app);
  }
  get resultScheduleNewGame(): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.socket.on('schedule-new-game', (status) => {
        observer.next(status);
      });
    });
  }

  checkNewGameTimeBusy(app: AppGame): void {
    this.socket.emit('schedule-new-game-check-time', app.nextGameData);
  }
  get resultCheckNewGameTimeBusy(): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.socket.on('schedule-new-game-check-time', (status) => {
        observer.next(status);
      });
    });
  }

  setGameState(appID: number | string, state: number): void {
    this.socket.emit('game-change-state', appID, state);
  }
  get onAppEventChange(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('app-change-state', (game, state) => {
        observer.next({
          state: state.slice(state.indexOf(':') + 1),
          game: game.slice(game.indexOf(':') + 1),
        });
      });
    });
  }

  getAllGames(full: boolean = false): void {
    if (full) {
      this.socket.emit('games-full-list');
    } else {
      this.socket.emit('games-short-list');
    }
  }
  get allGamesShortList(): Observable<ShortGamesListInterface[]> {
    return new Observable<ShortGamesListInterface[]>((observer) => {
      this.socket.on('games-short-list', (games) => {
        observer.next(games);
      });
    });
  }

  getGameDetailsByID(gameID: number): void {
    this.socket.emit('games-detail-byid', gameID);
  }
  get gameDetailsByID(): Observable<GameDetailsInterface> {
    return new Observable<GameDetailsInterface>((observer) => {
      this.socket.on('games-detail-byid', (mapSrc) => {
        observer.next(mapSrc);
      });
    });
  }

  getTemplateStageDetailsByCode(templateCode: string): void {
    this.socket.emit('template-stage-detail-bycode', templateCode);
  }
  get templateStageDetailsByCode(): Observable<TemplateStageDetailInterface[]> {
    return new Observable<TemplateStageDetailInterface[]>((observer) => {
      this.socket.on('template-stage-detail-bycode', (stages) => {
        const stageDetail = stages.map((stage) => {
          return {
            stageID: stage.stid,
            stageCode: stage.stcode,
            stageTitle: stage.sttitle,
            questionIcon: stage.questionicon,
            portalIcon: stage.portalicon,
            map: {
              src: stage.mapsrc,
              posX: stage.mapposx,
              posY: stage.mapposy,
            },
            stageModule: {
              code: stage.smcode,
              description: stage.smdescription,
              title: stage.smtitle,
              uuid: stage.smuuid
            }
          };
        });

        observer.next(stageDetail);
      });
    });
  }

  getGameStageDetailsByCode(app: number, stageCode: string): void {
    this.socket.emit('activegame-stage-detail-bycode', app, stageCode);
  }
  get gameStageDetailsByCode(): Observable<TemplateStageDetailInterface[]> {
    return new Observable<TemplateStageDetailInterface[]>((observer) => {
      this.socket.on('activegame-stage-detail-bycode', (stages) => {
        const stageDetail = stages.map((stage) => {
          return {
            stageID: stage.stid,
            stageCode: stage.stcode,
            stageTitle: stage.sttitle,
            questionIcon: stage.questionicon,
            portalIcon: stage.portalicon,
            map: {
              src: stage.mapsrc,
              posX: stage.mapposx,
              posY: stage.mapposy,
            },
            stageModule: {
              code: stage.smcode,
              description: stage.smdescription,
              title: stage.smtitle,
              uuid: stage.smuuid
            }
          };
        });

        observer.next(stageDetail);
      });
    });
  }

  getGameDetailsQuestionsShort(gameID: number): void {
    this.socket.emit('games-detail-questions-short', gameID);
  }
  get gameDetailsQuestionsShort(): Observable<GameDetailsShortQuestions[]> {
    return new Observable<GameDetailsShortQuestions[]>((observer) => {
      this.socket.on('games-detail-questions-short', (questions) => {
        questions = questions.map((question) => {
          question.code = question.code.replace('/', '_');
          return question;
        });
        observer.next(questions);
      });
    });
  }

  getTemplateQuestions(gameID: number): void {
    this.socket.emit('template-questions-detail-byid', gameID);
  }
  get templateQuestions(): Observable<TemplateQuestionInterface[]> {
    return new Observable<TemplateQuestionInterface[]>((observer) => {
      this.socket.on('template-questions-detail-byid', (questions) => {
        observer.next(questions);
      });
    });
  }

  getActiveGameQuestions(gameID: number): void {
    this.socket.emit('activegame-questions-detail-byid', gameID);
  }
  get activeGameQuestions(): Observable<TemplateQuestionInterface[]> {
    return new Observable<TemplateQuestionInterface[]>((observer) => {
      this.socket.on('activegame-questions-detail-byid', (questions) => {
        observer.next(questions);
      });
    });
  }

  getActiveGamePortals(gameID: number): void {
    this.socket.emit('activegame-portals-detail-byid', gameID);
  }
  get activeGamePortals(): Observable<TemplatePortalInterface[]> {
    return new Observable<TemplatePortalInterface[]>((observer) => {
      this.socket.on('activegame-portals-detail-byid', (portals) => {
        portals.moveFrom = portals.movefrom;
        observer.next(portals);
      });
    });
  }

  getTemplatePortals(gameID: number): void {
    this.socket.emit('template-portals-detail-byid', gameID);
  }
  get templatePortals(): Observable<TemplatePortalInterface[]> {
    return new Observable<TemplatePortalInterface[]>((observer) => {
      this.socket.on('template-portals-detail-byid', (portals) => {
        portals.moveFrom = portals.movefrom;
        observer.next(portals);
      });
    });
  }

  getGameDetailsQuestionsFullByStage(gameID: number, stageCode: string): void {
    this.socket.emit('games-detail-questions-full-bystage', gameID, stageCode.replace('_', '/'));
  }
  get gameDetailQuestionsFullByStage(): Observable<GameDetailsQuestionsFull[]> {
    return new Observable<GameDetailsQuestionsFull[]>((observer) => {
      this.socket.on('games-detail-questions-full-bystage', (questions, error) => {
        observer.next(questions);
      });
    });
  }

  getGameDetailsPortalsShort(gameID: number): void {
    this.socket.emit('games-detail-portals-short', gameID);
  }
  get gameDetailsPortalsShort(): Observable<GameDetailsShortPortals[]> {
    return new Observable<GameDetailsShortPortals[]>((observer) => {
      this.socket.on('games-detail-portals-short', (portals) => {
        portals = portals.map((portal) => {
          portal.code = portal.code.replace('/', '_');
          return portal;
        });
        observer.next(portals);
      });
    });
  }

  getGameDetailsPortalsFullByStage(gameID: number, stageCode: string): void {
    this.socket.emit('games-detail-portals-full-bystage', gameID, stageCode.replace('_', '/'));
  }
  get gameDetailPortalsFullByStage(): Observable<GameDetailsPortalsFull[]> {
    return new Observable<GameDetailsPortalsFull[]>((observer) => {
      this.socket.on('games-detail-portals-full-bystage', (questions, error) => {
        observer.next(questions);
      });
    });
  }

  getGameTemplatesData(gameID: string): void {
    this.socket.emit('game-template-fulldata', gameID);
  }
  get gameTemplatesData(): Observable<GameTemplateMainInfoInterface> {
    return new Observable<GameTemplateMainInfoInterface>((observer) => {
      this.socket.on('game-template-fulldata', (data) => {
        observer.next(data);
      });
    });
  }

  getTemplateTypes(): void {
    this.socket.emit('get-template-types');
  }
  get templateTypes(): Observable<GameType[]> {
    return new Observable<GameType[]>((observer) => {
      this.socket.on('get-template-types', (types) => {
        observer.next(types);
      });
    });
  }

  doUpdateTemplateField(gameCode: string, field: string, value: string): void {
    this.socket.emit('update-template-field', gameCode, field, value);
  }
  get updateTemplateField(): Observable<boolean> {
    return new Observable<boolean>((observer) => {
      this.socket.on('update-template-field', (result) => {
        observer.next(result);
      });
    });
  }

  getCurrentActiveGameCode(): void {
    this.socket.emit('get-active-gamecode');
  }
  get currentActiveGameCode(): Observable<ActiveGameInterface> {
    return new Observable<ActiveGameInterface>((observer) => {
      this.socket.on('get-active-gamecode', (activeGame) => {
        observer.next(activeGame);
      });
    });
  }


  getDemoCode(): void {
    this.socket.emit('get-demo-code', 3);
  }
  get demoCode(): Observable<{id: number, code: string}> {
    return new Observable<{id: number, code: string}>((observer) => {
      this.socket.on('get-demo-code', (result) => {
        observer.next(result);
      });
    });
  }



  dropTemplateQuestion(id: number): void {
    this.socket.emit('drop-template-question', id);
  }

}
