import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { io, Socket } from 'socket.io-client';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs';
import {UserRole} from './interfaces/user-roles';
import {ActiveGameUser} from './interfaces/active-game-user';
import {UserAccount} from './interfaces/user-account';

@Injectable({
  providedIn: 'root'
})
export class SocketSettingsService {
  socket: Socket;
  constructor(private httpClient: HttpClient) {
    this.socket = io(`${environment.socketScheme}://${environment.socketHost}:${environment.socketPort}`);
    this.onResponse().subscribe((msg) => {
      console.log(msg);
    });
  }

  onResponse(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('response', msg => {
        observer.next(msg);
      });
    });
  }

  // getDataFromTemplateGame(gameCode: string): void {
  //   this.socket.emit('get-gametemplate-info', gameCode);
  // }
  // get dataFromTemplateGame(): Observable<GameTemplate[]> {
  //   return new Observable<GameTemplate[]>(observer => {
  //     this.socket.on('get-gametemplate-info', (stages, portals, questions, count) => {
  //       stages.map(stage => {
  //         stage.portals = portals.filter(portal => portal.code === stage.code);
  //         stage.questions = questions.filter(question => question.code === stage.code);
  //         stage.makedGames = count;
  //       });
  //       observer.next(stages);
  //     });
  //   });
  // }

  addRole(role: UserRole): void {
    this.socket.emit('settings-add-role', role.title, role.code);
  }
  dropRole(id: number): void {
    this.socket.emit('settings-drop-role', id);
  }
  setDefaultRole(id: number): void {
    this.socket.emit('settings-setdefault-role', id);
  }

  getRoles(): void {
    this.socket.emit('settings-get-roles');
  }
  get roles(): Observable<UserRole[]> {
    return new Observable<UserRole[]>(observer => {
      this.socket.on('settings-get-roles', (roles) => {
        observer.next(roles);
      });
    });
  }

  getActiveGamePlayers(): void {
    this.socket.emit('settings-get-activegame-users');
  }
  get activeGamePlayers(): Observable<ActiveGameUser[]> {
    return new Observable<ActiveGameUser[]>(observer => {
      this.socket.on('settings-get-activegame-users', (users) => {
        observer.next(users);
      });
    });
  }

  getUsers(): void {
    this.socket.emit('settings-get-users');
  }
  get users(): Observable<UserAccount[]> {
    return new Observable<UserAccount[]>(observer => {
      this.socket.on('settings-get-users', (users) => {
        observer.next(users);
      });
    });
  }

  updateUserAccount(user: UserAccount): void {
    this.socket.emit('settings-update-account', user);
  }
}
