import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { io, Socket } from 'socket.io-client';
import {environment} from '../environments/environment';
import {Observable, Subscription} from 'rxjs';
import {UserRole} from './interfaces/user-roles';
import {UserAccount} from './interfaces/user-account';
import {AuthService} from '@auth0/auth0-angular';
import {ProfileAuth} from './interfaces/profile-auth';

@Injectable({
  providedIn: 'root'
})
export class SocketAuthService {
  auth0StateSubscription: Subscription;

  socket: Socket;
  account: UserAccount;

  constructor(private httpClient: HttpClient, public auth0: AuthService) {
    this.socket = io(`${environment.socketScheme}://${environment.socketHost}:${environment.socketPort}`);

    this.onResponse().subscribe((msg) => {
      console.log(msg);
    });

    this.auth0StateSubscription = this.auth0.isAuthenticated$.subscribe((authState) => {
      if (authState) {
        this.auth0.user$.subscribe((profile) => {
          this.getUserInfo(profile.email);
        });
      } else {

      }
    });
  }

  onResponse(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('response', msg => {
        observer.next(msg);
      });
    });
  }

  registerWithEmail(account: UserAccount): void {
    this.socket.emit('auth-register-email', account);
  }
  get resultRegisterWithEmail(): Observable<boolean> {
    return new Observable<boolean>((observer) => {
      this.socket.on('auth-register-email', (result) => {
        observer.next(result);
      });
    });
  }

  getUserInfo(email: string): void {
    this.socket.emit('auth-getuser-info', email);
  }

  get userInfo(): Observable<UserAccount> {
    return new Observable<UserAccount>(observer => {
      this.socket.on('auth-getuser-info', (account) => {
        account.userRole = {
          title: account.title,
          code: account.code,
          isdefault: account.isdefault
        };

        this.account = account;
        this.account.id = account.uid;
        this.account.role = account.rid;
        observer.next(account);
      });
    });
  }

  apiRegister(authFields: ProfileAuth): Observable<any> {
    return this.httpClient.post('https://ratatosk-studio.eu.auth0.com/dbconnections/signup', {
      // client_id: 'y9SgYBjBQt6EumqqwqvMgOfV3Re4uuEw',
      client_id: 'MQuoqfSdb7yxeb87rtikAq0mwpzit3UO',
      email: authFields.email,
      password: authFields.password,
      connection: 'Username-Password-Authentication',
      username: authFields.username,
      nickname: authFields.username
    });
  }

}
