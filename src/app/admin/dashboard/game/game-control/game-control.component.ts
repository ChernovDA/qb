import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppGame} from '../../../../interfaces/app-game';
import {Subscription} from 'rxjs';
import {SocketService} from '../../../../socket.service';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.less']
})
export class GameControlComponent implements OnInit, OnDestroy {
  upcomingGamesList: AppGame;
  index = 0;
  appGamesSubscription: Subscription;

  constructor(public socketService: SocketService) {
    this.socketService.getAppGames();
    this.subscribeChangeState();
  }

  ngOnInit(): void {
    const currentDateTimestamp = Math.floor(Date.now() / 1000);
    this.appGamesSubscription = this.socketService.appGames.subscribe((apps) => {
      this.upcomingGamesList = apps
        .filter((game) => Math.floor(Date.parse(game.nextGameData) / 1000 + 60 * 60) > currentDateTimestamp)
        .find(Boolean);
    });

  }

  ngOnDestroy(): void {
    this.appGamesSubscription.unsubscribe();
  }

  onIndexChange(index: number): void {
    this.index = index;
    switch (index) {
      case 0: {
        this.socketService.setGameState(this.upcomingGamesList.id, 2);
        this.upcomingGamesList.state = {
          announce: true,
          preparing: false,
          active: false,
          gameOver: false
        };
        break;
      }
      case 1: {
        this.socketService.setGameState(this.upcomingGamesList.id, 4);
        this.upcomingGamesList.state = {
          announce: false,
          preparing: true,
          active: false,
          gameOver: false
        };
        break;
      }
      case 2: {
        this.socketService.setGameState(this.upcomingGamesList.id, 1);
        this.upcomingGamesList.state = {
          announce: false,
          preparing: false,
          active: true,
          gameOver: false
        };
        break;
      }
      case 3: {
        this.socketService.setGameState(this.upcomingGamesList.id, 3);
        this.upcomingGamesList.state = {
          announce: false,
          preparing: false,
          active: false,
          gameOver: true
        };
        break;
      }
    }
  }

  subscribeChangeState(): void {
    this.socketService.onAppEventChange.subscribe((event) => {
      if (String(this.upcomingGamesList.id) === event.game) {
        switch (event.state) {
          case '2': {
            this.upcomingGamesList.state = {
              announce: true,
              preparing: false,
              active: false,
              gameOver: false
            };
            break;
          }
          case '4': {
            this.upcomingGamesList.state = {
              announce: false,
              preparing: true,
              active: false,
              gameOver: false
            };
            break;
          }
          case '1': {
            this.upcomingGamesList.state = {
              announce: false,
              preparing: false,
              active: true,
              gameOver: false
            };
            break;
          }
          case '3': {
            this.upcomingGamesList.state = {
              announce: false,
              preparing: false,
              active: false,
              gameOver: true
            };
            break;
          }
        }
      }
    });
  }
}
