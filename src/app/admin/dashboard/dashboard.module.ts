import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { GameControlComponent } from './game/game-control/game-control.component';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import {AuthGuard} from '@auth0/auth0-angular';

@NgModule({
  declarations: [
    DashboardComponent,
    GameControlComponent,
  ],
  imports: [
    CommonModule,
    NzLayoutModule,
    NzMenuModule,
    NzGridModule,
    NzStepsModule,
    NzTypographyModule,
    RouterModule.forChild([
      {
        path: '', component: DashboardComponent, children: [
          {
            path: 'auth',
            loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
          },
          {
            path: 'schedule',
            loadChildren: () => import('./schedule/schedule.module').then(m => m.ScheduleModule),
            canActivate: [AuthGuard]
          },
          {
            path: 'bd',
            loadChildren: () => import('./bd/bd.module').then(m => m.BdModule),
            canActivate: [AuthGuard]
          },
          {
            path: 'users',
            loadChildren: () => import('./users/users.module').then(m => m.UsersModule),
            canActivate: [AuthGuard]
          },
          {
            path: 'game-control',
            component: GameControlComponent,
            canActivate: [AuthGuard]
          },
          {
            path: 'games',
            loadChildren: () => import('./games/games.module').then(m => m.GamesModule),
            canActivate: [AuthGuard]
          }
        ],
      }
    ]),
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardModule { }
