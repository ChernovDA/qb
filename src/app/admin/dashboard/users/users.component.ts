import {Component, OnDestroy, OnInit} from '@angular/core';
import {SocketSettingsService} from '../../../socket-settings.service';
import {ActiveGameUser} from '../../../interfaces/active-game-user';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit, OnDestroy {
  activeGameUsers: ActiveGameUser[] = [];

  activeGamePlayersSubscription: Subscription;

  constructor(private socketSettings: SocketSettingsService) {

  }

  ngOnInit(): void {
    this.socketSettings.getActiveGamePlayers();
    this.activeGamePlayersSubscription = this.socketSettings.activeGamePlayers.subscribe((activeGamePlayers) => {
      this.activeGameUsers = activeGamePlayers.map(player => {
        if (player.question === null) {
          player.question = '';
        }
        return player;
      });

    });
  }

  ngOnDestroy(): void {
    this.activeGamePlayersSubscription?.unsubscribe();
  }
}
