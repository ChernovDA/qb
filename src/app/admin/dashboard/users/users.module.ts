import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';

import { ListComponent } from './list/list.component';
import { SettingsComponent } from './settings/settings.component';
import { UsersComponent } from './users.component';

import { NzIconModule } from 'ng-zorro-antd/icon';
import { IconDefinition } from '@ant-design/icons-angular';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzSelectModule } from 'ng-zorro-antd/select';

import {
  PlusSquareTwoTone,
  ArrowLeftOutline,
  DeleteOutline
} from '@ant-design/icons-angular/icons';

const icons: IconDefinition[] = [
  PlusSquareTwoTone,
  ArrowLeftOutline,
  DeleteOutline
];

@NgModule({
  declarations: [
    ListComponent,
    SettingsComponent,
    UsersComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    NzDividerModule,
    NzPageHeaderModule,
    NzIconModule.forChild(icons),
    NzGridModule,
    NzFormModule,
    NzListModule,
    NzInputModule,
    NzNotificationModule,
    NzTableModule,
    NzSelectModule,
    RouterModule.forChild([
      {
        path: '', component: UsersComponent
      },
      {
        path: 'list',
        component: ListComponent
      },
      {
        path: 'settings',
        component: SettingsComponent
      }
    ]),
    NzButtonModule,
  ],
  exports: [
    RouterModule
  ]
})
export class UsersModule { }
