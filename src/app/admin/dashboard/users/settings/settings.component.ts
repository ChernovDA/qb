import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserRole} from '../../../../interfaces/user-roles';
import {SocketSettingsService} from '../../../../socket-settings.service';
import {Subscription} from 'rxjs';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.less']
})
export class SettingsComponent implements OnInit, OnDestroy {
  rolesFG!: FormGroup;
  roles: UserRole[] = null;
  rolesSubscription: Subscription;

  constructor(private fb: FormBuilder, public socket: SocketSettingsService, private notification: NzNotificationService) {
    console.log('setting');
    this.rolesFG = this.fb.group({
      title: [null, Validators.required],
      code: [null, Validators.required],
      isdefault: [false]
    });
    this.socket.getRoles();
  }

  ngOnInit(): void {
    this.getRoles();
  }

  ngOnDestroy(): void {
    if (this.rolesSubscription !== undefined) {
      this.rolesSubscription.unsubscribe();
    }
  }

  getRoles(): void {
    if (this.rolesSubscription !== undefined) {
      this.rolesSubscription.unsubscribe();
    }
    this.rolesSubscription = this.socket.roles.subscribe((roles) => {
      this.roles = roles;
    });
  }

  addRole(): void {
    for (const i in this.rolesFG.controls) {
      if (this.rolesFG.controls.hasOwnProperty(i)) {
        this.rolesFG.controls[i].markAsDirty();
        this.rolesFG.controls[i].updateValueAndValidity();
      }
    }

    if (this.rolesFG.valid) {
      this.socket.addRole(this.rolesFG.value);
      this.roles.push(this.rolesFG.value);

      for (const i in this.rolesFG.controls) {
        if (this.rolesFG.controls.hasOwnProperty(i)) {
          this.rolesFG.controls[i].reset();
        }
      }
    }
  }

  dropRole(e: Event, id: number): void {
    e.preventDefault();
    this.socket.dropRole(id);
    this.notification.create(
      'success',
      'Удаление роли',
      `Роль "${this.roles.filter((role) => role.id === id)[0].title}" успешно удалена`
    );
    this.roles = this.roles.filter((role) => role.id !== id);

  }

  setDefaultRole(e: Event, id: number): void {
    e.preventDefault();
    this.socket.setDefaultRole(id);
    this.notification.create(
      'success',
      'Установка по умолчанию',
      `Роль "${this.roles.filter((role) => role.id === id)[0].title}" будет назначена по умолчанию для новых регистраций пользователей`
    );
    this.roles.map((role) => {
      role.isdefault = role.id === id;
      return role;
    });
  }
}
