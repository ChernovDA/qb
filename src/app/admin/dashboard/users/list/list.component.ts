import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserRole} from '../../../../interfaces/user-roles';
import {SocketSettingsService} from '../../../../socket-settings.service';
import {Subscription} from 'rxjs';
import {UserAccount} from '../../../../interfaces/user-account';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit, OnDestroy {
  roles: UserRole[] = [];
  users: UserAccount[] = [];

  rolesSubscription: Subscription;
  usersSubscription: Subscription;
  constructor(private socketSettings: SocketSettingsService) {}

  ngOnInit(): void {
    this.socketSettings.getRoles();
    this.socketSettings.getUsers();
    this.rolesSubscription = this.socketSettings.roles.subscribe((roles) => {
      this.roles = roles;

      this.usersSubscription = this.socketSettings.users.subscribe((users) => {
        this.users = users;
      });
    });
  }

  ngOnDestroy(): void {
    this.rolesSubscription?.unsubscribe();
    this.usersSubscription?.unsubscribe();
  }

  changeUserRole(user: UserAccount): void {
    if (user.email !== 'ch3rnov.denis@gmail.com') {
      this.socketSettings.updateUserAccount(user);
    }
  }
}
