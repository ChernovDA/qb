import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';

import { NzListModule } from 'ng-zorro-antd/list';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { IconDefinition } from '@ant-design/icons-angular';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzCalendarModule } from 'ng-zorro-antd/calendar';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzCardModule} from 'ng-zorro-antd/card';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { SharedModule } from '../../../shared/shared.module';

import { GamesListComponent } from './games-list/games-list.component';
import { GameCreateComponent } from './game-create/game-create.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { GamePortalsComponent } from './game-detail/game-portals/game-portals.component';
import { GameQuestionsComponent } from './game-detail/game-questions/game-questions.component';
import { GameStagesComponent } from './game-detail/game-stages/game-stages.component';
import { GamePortalsDetailComponent } from './game-detail/game-portals/game-portals-detail/game-portals-detail.component';
import { GameQuestionsDetailComponent } from './game-detail/game-questions/game-questions-detail/game-questions-detail.component';
import { GamesTemplatesComponent } from './games-templates/games-templates.component';
import { GamesTemplateQuestionsComponent } from './games-templates/games-template-questions/games-template-questions.component';
import { GamesTemplatePortalsComponent } from './games-templates/games-template-portals/games-template-portals.component';
import { GamesTemplateStagesComponent } from './games-templates/games-template-stages/games-template-stages.component';
import { GamesTemplateCreateComponent } from './games-templates/games-template-create/games-template-create.component';
import {GamesTemplateEditComponent} from './games-templates/games-template-edit/games-template-edit.component';

import {
  FundTwoTone,
  QuestionCircleTwoTone,
  ExclamationCircleTwoTone,
  AppstoreTwoTone,
  ArrowLeftOutline,
  EditOutline,
  FolderViewOutline,
  PlusCircleOutline,
  AppstoreOutline,
  PlusOutline,
  FileImageTwoTone,
  DeleteOutline,
  CheckOutline
} from '@ant-design/icons-angular/icons';


const icons: IconDefinition[] = [
  FundTwoTone,
  QuestionCircleTwoTone,
  ExclamationCircleTwoTone,
  AppstoreTwoTone,
  ArrowLeftOutline,
  EditOutline,
  FolderViewOutline,
  PlusCircleOutline,
  AppstoreOutline,
  PlusOutline,
  FileImageTwoTone,
  DeleteOutline,
  CheckOutline
];

@NgModule({
  declarations: [
    GamesListComponent,
    GameCreateComponent,
    GameDetailComponent,
    GamePortalsComponent,
    GameQuestionsComponent,
    GameStagesComponent,
    GamePortalsDetailComponent,
    GameQuestionsDetailComponent,
    GamesTemplatesComponent,
    GamesTemplateQuestionsComponent,
    GamesTemplatePortalsComponent,
    GamesTemplateStagesComponent,
    GamesTemplateCreateComponent,
    GamesTemplateEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '', children: [
          { path: 'list', component: GamesListComponent },
          { path: 'add', component: GameCreateComponent },
          {
            path: 'detail/:id', component: GameDetailComponent, children: [
              {path: 'portals', component: GamePortalsComponent},
              {path: 'questions', component: GameQuestionsComponent},
              {path: 'stages', component: GameStagesComponent},
            ]
          },
          { path: 'detail/:id/:stage/questions', component: GameQuestionsDetailComponent },
          { path: 'detail/:id/:stage/portals', component: GamePortalsDetailComponent },
          { path: 'templates', component: GamesTemplatesComponent },
          { path: 'templates/edit/:id', component: GamesTemplateEditComponent },
          { path: 'templates/edit/:id/portals', component: GamesTemplatePortalsComponent },
          { path: 'templates/edit/:id/questions', component: GamesTemplateQuestionsComponent },
          { path: 'templates/edit/:id/stages', component: GamesTemplateStagesComponent },
          { path: 'templates/add', component: GamesTemplateCreateComponent }
        ]
      }
    ]),
    NzListModule,
    NzLayoutModule,
    NzPageHeaderModule,
    NzIconModule.forChild(icons),
    NzImageModule,
    NzTagModule,
    NzGridModule,
    NzCalendarModule,
    NzDividerModule,
    NzTableModule,
    NzButtonModule,
    NzToolTipModule,
    NzSwitchModule,
    NzCardModule,
    NzPopconfirmModule,
    NzUploadModule,
    NzInputModule,
    NzSelectModule,
    NzCollapseModule
  ],
  exports: [
    RouterModule
  ]
})
export class GamesModule { }
