import {Component, OnDestroy, OnInit} from '@angular/core';
import {SocketService} from '../../../../socket.service';
import {Subscription} from 'rxjs';
import {GameInfo} from '../../../../interfaces/game-info';
import {Router} from '@angular/router';
import {NzMessageService} from 'ng-zorro-antd/message';

@Component({
  selector: 'app-games-templates',
  templateUrl: './games-templates.component.html',
  styleUrls: ['./games-templates.component.less'],
  providers: [NzMessageService]
})
export class GamesTemplatesComponent implements OnInit, OnDestroy {
  loading: boolean;

  gamesTitlesSubscription: Subscription;

  gamesTitles: GameInfo[] = null;

  constructor(public socket: SocketService, public router: Router, private nzMessageService: NzMessageService) {
    this.loading = true;
    this.socket.getGamesTitles();
  }

  ngOnInit(): void {
    this.gamesTitlesSubscription = this.socket.gamesTitles.subscribe((titles) => {
      this.loading = false;
      this.gamesTitles = titles;
    });
  }

  ngOnDestroy(): void {
    if (this.gamesTitlesSubscription !== undefined) {
      this.gamesTitlesSubscription.unsubscribe();
    }

  }

  createUsingTemplate(id: string): void {
    this.router.navigate(['/admin/dashboard/schedule/add', id]);
  }

  dropTemplate(id: string): void {

  }

  cancel(): void {
  }
}
