import {Component, OnDestroy, OnInit} from '@angular/core';
import {SocketService} from '../../../../../socket.service';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {NzUploadChangeParam} from 'ng-zorro-antd/upload';
import { NzMessageService } from 'ng-zorro-antd/message';
import {
  GameTemplateMainInfoInterface,
  TemplatePortalInterface,
  TemplateQuestionInterface
} from '../../../../../interfaces/game-template-main-info';
import {GameType} from '../../../../../interfaces/game-type';
import {TemplateStageDetailInterface} from '../../../../../interfaces/template-stage-detail';

@Component({
  selector: 'app-games-template-edit',
  templateUrl: './games-template-edit.component.html',
  styleUrls: ['./games-template-edit.component.less'],
  providers: [NzMessageService]
})
export class GamesTemplateEditComponent implements OnInit, OnDestroy {
  gameTitle: string;
  paramsSubscription: Subscription;
  gameTemplatesDataSubscription: Subscription;
  templateTypesSubscription: Subscription;
  updateTemplateFieldSubscription: Subscription;
  templateDetailsStageSubscription: Subscription;
  templateDetailsQuestionsSubscription: Subscription;
  templateDetailsPortalsSubscription: Subscription;

  gameTemplateTitle: string;
  gameTemplateCode: string;
  gameType: GameType;
  templateTypes: GameType[] = [];
  templateStages: TemplateStageDetailInterface[] = [];
  templateQuestions: TemplateQuestionInterface[] = [];
  templatePortals: TemplatePortalInterface[] = [];

  fallback =
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg==';

  gameTemplate: GameTemplateMainInfoInterface;

  popupDropMessages: {
    title: string,
    ok: string,
    cancel: string
  };

  constructor(private location: Location,
              public socket: SocketService,
              public router: ActivatedRoute,
              private msg: NzMessageService
  ) {
    this.popupDropMessages = {
        title: 'Удалить? Это необратимо',
        ok: 'Подтверждаю',
        cancel: 'Ошибка! Отмена!'
    };


    this.paramsSubscription = this.router.paramMap.subscribe((params) => {
      this.socket.getGameTemplatesData(params.get('id'));
    });
    this.socket.getTemplateTypes();

  }

  ngOnInit(): void {
    this.templateTypesSubscription = this.socket.templateTypes.subscribe((types) => {
      this.templateTypes = types;
    });

    this.gameTemplatesDataSubscription = this.socket.gameTemplatesData.subscribe((templateData) => {
      this.gameTitle = templateData.title;
      this.gameTemplateTitle = templateData.title;
      this.gameTemplateCode = templateData.code;

      this.gameTemplate = templateData;
      this.gameType = {
        id: templateData.typeid,
        code: templateData.typecode,
        title: templateData.typetitle
      };

      this.gameTemplate.gameType = this.gameType;

      this.socket.getTemplateStageDetailsByCode(templateData.code);
      this.socket.getTemplateQuestions(templateData.id);
      this.socket.getTemplatePortals(templateData.id);
    });
    this.templateDetailsStageSubscription = this.socket.templateStageDetailsByCode.subscribe((stages) => {
      this.templateStages = stages;
    });
    this.templateDetailsQuestionsSubscription = this.socket.templateQuestions.subscribe((questions) => {
      this.templateQuestions = questions.map((question) => {
        if (question.answer !== null) {
          question.answers = question.answer.split('/');
        }
        return question;
      });
    });
    this.templateDetailsPortalsSubscription = this.socket.templatePortals.subscribe((portals) => {
      this.templatePortals = portals;
      console.log(this.templatePortals);
    });
  }

  ngOnDestroy(): void {
    this.paramsSubscription.unsubscribe();
    if (this.gameTemplatesDataSubscription !== undefined) {
      this.gameTemplatesDataSubscription.unsubscribe();
    }
    if (this.templateTypesSubscription !== undefined) {
      this.templateTypesSubscription.unsubscribe();
    }
    if (this.updateTemplateFieldSubscription !== undefined) {
      this.updateTemplateFieldSubscription.unsubscribe();
    }
    if (this.templateDetailsStageSubscription !== undefined) {
      this.templateDetailsStageSubscription.unsubscribe();
    }
    if (this.templateDetailsQuestionsSubscription !== undefined) {
      this.templateDetailsQuestionsSubscription.unsubscribe();
    }
    if (this.templateDetailsPortalsSubscription !== undefined) {
      this.templateDetailsPortalsSubscription.unsubscribe();
    }
  }

  onBack(): void {
    this.location.back();
  }

  handleChange({file, fileList}: NzUploadChangeParam): void {
    const status = file.status;
    console.log(status);
    console.log(file, fileList);

    switch (status) {
      case 'done': {
        break;
      }
      case 'uploading': {
        break;
      }
      case 'error': {

        break;
      }
      default: {
        console.log(file, fileList);
        break;
      }
    }
  }

  compareGameTypes = (o1: GameType, o2: GameType) => (o1 && o2 ? o1.code === o2.code : o1 === o2);

  updateTemplateTitle(): void {
    if (this.updateTemplateFieldSubscription !== undefined) {
      this.updateTemplateFieldSubscription.unsubscribe();
    }
    this.updateTemplateFieldSubscription = this.socket.updateTemplateField.subscribe((result) => {
      if (result) {
        this.gameTemplate.title = this.gameTemplateTitle;
      }
    });
    this.socket.doUpdateTemplateField(this.gameTemplate.code, 'title', this.gameTemplateTitle);

  }

  updateTemplateCode(): void {
    if (this.updateTemplateFieldSubscription !== undefined) {
      this.updateTemplateFieldSubscription.unsubscribe();
    }
    this.updateTemplateFieldSubscription = this.socket.updateTemplateField.subscribe((result) => {
      if (result) {
        this.gameTemplate.code = this.gameTemplateCode;
      }
    });
    this.socket.doUpdateTemplateField(this.gameTemplate.code, 'code', this.gameTemplateCode);
  }

  updateTemplateType(): void {
    const code = this.templateTypes.find((type) => type.code === this.gameType.code);
    if (this.updateTemplateFieldSubscription !== undefined) {
      this.updateTemplateFieldSubscription.unsubscribe();
    }
    this.updateTemplateFieldSubscription = this.socket.updateTemplateField.subscribe((result) => {
      if (result) {
        this.gameTemplate.gameType = code;
      }
    });

    this.socket.doUpdateTemplateField(this.gameTemplate.code, 'type', `${code.id}`);
  }

  dropQuestion(id: number): void {
    this.socket.dropTemplateQuestion(id);
    this.templateQuestions = this.templateQuestions.filter((question) => question.id !== id);
  }
}
