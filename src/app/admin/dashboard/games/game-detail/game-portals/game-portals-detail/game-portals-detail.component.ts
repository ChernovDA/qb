import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {SocketService} from '../../../../../../socket.service';
import {Subscription} from 'rxjs';
import {GameDetailsPortalsFull} from '../../../../../../interfaces/game-detail-fullinfo';

@Component({
  selector: 'app-game-portals-detail',
  templateUrl: './game-portals-detail.component.html',
  styleUrls: ['./game-portals-detail.component.less']
})
export class GamePortalsDetailComponent implements OnInit, OnDestroy {
  gameTitle: string;
  gameID: number;
  gameStage: string;
  routeParamsSubscription: Subscription;
  gameTitleSubscription: Subscription;
  gamePortalsSubscription: Subscription;
  gamePortals: GameDetailsPortalsFull[] = null;

  constructor(private location: Location,
              private activatedRoute: ActivatedRoute,
              public socket: SocketService) {
    this.routeParamsSubscription = activatedRoute.params.subscribe((params) => {
      this.gameID = params.id;
      this.gameStage = params.stage;
      this.socket.getGameTitle(params.id);
      this.socket.getGameDetailsPortalsFullByStage(params.id, params.stage);
    });
  }

  ngOnInit(): void {
    this.gameTitleSubscription = this.socket.gameTitle.subscribe((title) => {
      this.gameTitle = title;
    });
    this.gamePortalsSubscription = this.socket.gameDetailPortalsFullByStage.subscribe((portals) => {
      this.gamePortals = portals;
    });
  }

  ngOnDestroy(): void {
    this.routeParamsSubscription.unsubscribe();
    if (this.gameTitleSubscription !== undefined) {
      this.gameTitleSubscription.unsubscribe();
    }
    if (this.gamePortalsSubscription !== undefined) {
      this.gamePortalsSubscription.unsubscribe();
    }
  }

  onBack(): void {
    this.location.back();
  }
}
