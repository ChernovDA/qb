import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {SocketService} from '../../../../../../socket.service';
import {Subscription} from 'rxjs';
import {GameDetailsQuestionsFull} from '../../../../../../interfaces/game-detail-fullinfo';

@Component({
  selector: 'app-game-questions-detail',
  templateUrl: './game-questions-detail.component.html',
  styleUrls: ['./game-questions-detail.component.less']
})
export class GameQuestionsDetailComponent implements OnInit, OnDestroy {
  gameTitle: string;
  gameID: number;
  gameStage: string;
  routeParamsSubscription: Subscription;
  gameTitleSubscription: Subscription;
  gameQuestionsSubscription: Subscription;
  gameQuestions: GameDetailsQuestionsFull[] = null;

  constructor(private location: Location,
              private activatedRoute: ActivatedRoute,
              public socket: SocketService) {
    this.routeParamsSubscription = activatedRoute.params.subscribe((params) => {
      this.gameID = params.id;
      this.gameStage = params.stage;
      this.socket.getGameTitle(params.id);
      this.socket.getGameDetailsQuestionsFullByStage(params.id, params.stage);
    });
  }

  ngOnInit(): void {
    this.gameTitleSubscription = this.socket.gameTitle.subscribe((title) => {
      this.gameTitle = title;
    });
    this.gameQuestionsSubscription = this.socket.gameDetailQuestionsFullByStage.subscribe((questions) => {
      this.gameQuestions = questions.map((question) => {
        if (question.answer) {
          question.answers = question.answer.split('/');
        }
        return question;
      });
    });
  }

  ngOnDestroy(): void {
    this.routeParamsSubscription.unsubscribe();
    if (this.gameTitleSubscription !== undefined) {
      this.gameTitleSubscription.unsubscribe();
    }
    if (this.gameQuestionsSubscription !== undefined) {
      this.gameQuestionsSubscription.unsubscribe();
    }
  }

  onBack(): void {
    this.location.back();
  }

}
