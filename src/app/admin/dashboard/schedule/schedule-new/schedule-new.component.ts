import {Component, OnDestroy, OnInit} from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {FbService} from '../../../../fb.service';
import {AppGame} from '../../../../interfaces/app-game';
import {GameInfo} from '../../../../interfaces/game-info';
import {SocketService} from '../../../../socket.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-schedule-new',
  templateUrl: './schedule-new.component.html',
  styleUrls: ['./schedule-new.component.less']
})
export class ScheduleNewComponent implements OnInit, OnDestroy {
  gameData: Subscription;
  appGamesSubscription: Subscription;
  gamesTitlesSubscription: Subscription;
  checkTimeBusySubscription: Subscription;
  routeParams: Subscription;

  addNewGameForm!: FormGroup;
  gamesList: Array<AppGame> = [];
  gamesTitles: Array<GameInfo> = [];
  chosenGameStats: {
    maps: number,
    portals: number,
    questions: number,
    makedGames: number
  };

  selectedGame = undefined;

  successfulAddedToSchedule: boolean;
  errorAddingToSchedule: boolean;
  canSchedule: boolean;
  canScheduleBtnText: string;

  constructor(private location: Location,
              private fb: FormBuilder,
              public fbService: FbService,
              public socketService: SocketService,
              public route: ActivatedRoute
  ) {
    this.chosenGameStats = {
      maps: 0,
      portals: 0,
      questions: 0,
      makedGames: 0
    };

    this.successfulAddedToSchedule = false;
    this.errorAddingToSchedule = false;
    this.canSchedule = false;
    this.canScheduleBtnText = 'Заполните все необходимые поля';
    this.socketService.getAppGames();
    this.socketService.getGamesTitles();
  }

  ngOnInit(): void {
    this.addNewGameForm = this.fb.group({
      activeGame: [null, Validators.required],
      authModule: ['1', Validators.required],
      nextGameData: [null, Validators.required],
      profileModule: ['1', Validators.required],
      state: [
        {
          active: false,
          announce: true,
          gameOver: false,
          preparing: false
        }
      ]
    });
    this.socketService.appGames.subscribe((apps) => {
      this.gamesList = apps;
    });
    this.socketService.gamesTitles.subscribe((gamesTitles) => {
      this.gamesTitles = gamesTitles;
      this.routeParams = this.route.paramMap.subscribe((params) => {
        console.log(params.get('gameid'));
        if (params.get('gameid') != null) {
          this.selectedGame = gamesTitles.find(title => title.id === params.get('gameid')).id;
        }
      });
    });

    // Firebase
    // this.fbService.gamesListObs.subscribe((games) => {
    //   games.sort((a, b) => {
    //     if (a.nextGameData < b.nextGameData) {
    //       return 1;
    //     }
    //     if (a.nextGameData > b.nextGameData) {
    //       return -1;
    //     }
    //     return 0;
    //   });
    //   this.gamesList = games;
    // });
    // this.fbService.gamesTitlesObs.subscribe((gamesTitles) => {
    //   this.gamesTitles = gamesTitles.filter((title) => title.id !== 'list');
    // });
  }

  ngOnDestroy(): void {
    if (this.gameData !== undefined) {
      this.gameData.unsubscribe();
    }
    if (this.appGamesSubscription !== undefined) {
      this.appGamesSubscription.unsubscribe();
    }
    if (this.gamesTitlesSubscription !== undefined) {
      this.gamesTitlesSubscription.unsubscribe();
    }
    if (this.checkTimeBusySubscription !== undefined) {
      this.checkTimeBusySubscription.unsubscribe();
    }
    if (this.routeParams !== undefined) {
      this.routeParams.unsubscribe();
    }
  }

  onBack(): void {
    this.location.back();
  }

  addGame(): void {
    for (const i in this.addNewGameForm.controls) {
      if (this.addNewGameForm.controls.hasOwnProperty(i)) {
        this.addNewGameForm.controls[i].markAsDirty();
        this.addNewGameForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.addNewGameForm.valid) {
      this.socketService.resultScheduleNewGame.subscribe((status) => {
        this.successfulAddedToSchedule = status;
      });
      this.socketService.scheduleNewGame(this.addNewGameForm.value);

      // Firebase engine
      // this.fbService.addGameToSchedule(this.addNewGameForm.value).then(() => {
      //   this.successfulAddedToSchedule = true;
      // })
      // .catch((error) => {
      //   console.log(error);
      //   this.errorAddingToSchedule = true;
      // });
    }
  }

  onChooseGame(gameID: string): void {
    if (this.addNewGameForm.value.nextGameData !== null) {
      this.timeAvailable();
    } else {
      this.canSchedule = false;
      this.canScheduleBtnText = 'Время начала не выбрано';
    }
    this.socketService.getDataFromTemplateGame(gameID);

    if (this.gameData !== undefined) {
      this.gameData.unsubscribe();
    }

    // Получение статистических данных
    this.gameData = this.socketService.dataFromTemplateGame.subscribe((data) => {
      this.chosenGameStats.maps = data.length;
      this.chosenGameStats.portals = 0;
      this.chosenGameStats.questions = 0;
      this.chosenGameStats.makedGames = data[0].makedGames;
      data.forEach((stage) => {
        this.chosenGameStats.portals += stage.portals.length;
        this.chosenGameStats.questions += stage.questions.length;
      });
    });
  }

  timeAvailable(): void {
    if (this.addNewGameForm.value.activeGame !== null) {
      if (this.checkTimeBusySubscription !== undefined) {
        this.checkTimeBusySubscription.unsubscribe();
      }

      const t = new Date(this.addNewGameForm.value.nextGameData);
      this.addNewGameForm.patchValue({
        nextGameData: `${t.getFullYear()}-` +
          `${t.getMonth() + 1 < 10 ? '0' + (t.getMonth() + 1) : (t.getMonth() + 1)}-` +
          `${t.getDate() < 10 ? '0' + t.getDate() : t.getDate()} ` +
          `${t.getHours()}:` +
          `${t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes()}:` +
          `${t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds()}`
      });

      this.socketService.checkNewGameTimeBusy(this.addNewGameForm.value);
      // Проверка даты и времени на занятость
      this.checkTimeBusySubscription = this.socketService.resultCheckNewGameTimeBusy.subscribe((isTimeAvailable) => {
        if (isTimeAvailable) {
          this.canSchedule = true;
          this.canScheduleBtnText = 'Запланировать игру';
        } else {
          this.canSchedule = false;
          this.canScheduleBtnText = 'Выбранное время уже занято';
        }
      });
    } else {
      this.canSchedule = false;
      this.canScheduleBtnText = 'Игра не выбрана';
    }
  }
}
