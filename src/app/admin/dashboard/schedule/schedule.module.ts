import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { IconDefinition } from '@ant-design/icons-angular';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { ReactiveFormsModule} from '@angular/forms';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzTimePickerModule } from 'ng-zorro-antd/time-picker';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzListModule } from 'ng-zorro-antd/list';

import { FrontListingScheduleComponent } from './front-listing-schedule/front-listing-schedule.component';
import { SchedulePastGamesComponent } from './schedule-past-games/schedule-past-games.component';
import { ScheduleNewComponent } from './schedule-new/schedule-new.component';

import {
  PlusSquareTwoTone,
  ArrowLeftOutline,
  DeleteOutline
} from '@ant-design/icons-angular/icons';

const icons: IconDefinition[] = [
  PlusSquareTwoTone,
  ArrowLeftOutline,
  DeleteOutline
];

@NgModule({
  declarations: [
    FrontListingScheduleComponent,
    SchedulePastGamesComponent,
    ScheduleNewComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '', component: FrontListingScheduleComponent
      },
      {
        path: 'past', component: SchedulePastGamesComponent
      },
      {
        path: 'add', component: ScheduleNewComponent
      },
      {
        path: 'add/:gameid', component: ScheduleNewComponent
      }
    ]),
    NzGridModule,
    NzDividerModule,
    NzButtonModule,
    NzIconModule.forChild(icons),
    NzPageHeaderModule,
    NzFormModule,
    NzInputModule,
    NzSelectModule,
    NzDatePickerModule,
    NzTimePickerModule,
    NzStatisticModule,
    NzResultModule,
    NzCardModule,
    NzListModule
  ],
  exports: [
    RouterModule
  ]
})
export class ScheduleModule { }
