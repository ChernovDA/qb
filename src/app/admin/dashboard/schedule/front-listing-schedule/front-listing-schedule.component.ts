import {Component, OnDestroy, OnInit} from '@angular/core';
import {FbService} from '../../../../fb.service';
import {AppGame} from '../../../../interfaces/app-game';
import { NzButtonSize } from 'ng-zorro-antd/button';
import {SocketService} from '../../../../socket.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-front-listing-schedule',
  templateUrl: './front-listing-schedule.component.html',
  styleUrls: ['./front-listing-schedule.component.less']
})
export class FrontListingScheduleComponent implements OnInit, OnDestroy {
  appGamesSubscription: Subscription;

  gamesList: Array<AppGame> = [];
  upcomingGamesList: Array<AppGame> = [];
  size: NzButtonSize = 'large';
  currentDateTimestamp: number;

  constructor(/*public fbService: FbService, */public socket: SocketService) {
    this.currentDateTimestamp = Math.floor(Date.now() / 1000);
    this.socket.getAppGames();
  }

  ngOnInit(): void {
    // v2. for PostgreSQL
    this.appGamesSubscription = this.socket.appGames.subscribe((apps) => {
      this.gamesList = apps.filter((game) => Math.floor(Date.parse(game.nextGameData) / 1000) < this.currentDateTimestamp);
      this.upcomingGamesList = apps
        .filter((game) => Math.floor(Date.parse(game.nextGameData) / 1000) > this.currentDateTimestamp);
    });

    // v1. for firebase
    /*
    this.fbService.gamesListObs.subscribe((games) => {
      this.gamesList = games
        .filter((game) => Math.floor(Date.parse(game.nextGameData) / 1000) < this.currentDateTimestamp)
        .sort((a, b) => {
          if (a.nextGameData < b.nextGameData) {
            return 1;
          }
          if (a.nextGameData > b.nextGameData) {
            return -1;
          }
          return 0;
        });

      this.upcomingGamesList = games
        .filter((game) => Math.floor(Date.parse(game.nextGameData) / 1000) > this.currentDateTimestamp)
        .sort((a, b) => {
          if (a.nextGameData < b.nextGameData) {
            return 1;
          }
          if (a.nextGameData > b.nextGameData) {
            return -1;
          }
          return 0;
        });
    });
    */
  }

  ngOnDestroy(): void {
    this.appGamesSubscription.unsubscribe();
  }

  editThis() {

  }
}
