import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {SocketAuthService} from '../../../../socket-auth.service';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-admin-register',
  templateUrl: './admin-register.component.html',
  styleUrls: ['./admin-register.component.less']
})
export class AdminRegisterComponent implements OnInit {
  formRegister!: FormGroup;

  constructor(private fb: FormBuilder, public socket: SocketAuthService, public auth: AuthService) { }

  ngOnInit(): void {
    this.formRegister = this.fb.group({
      email: [null, Validators.required, Validators.email],
      username: [null, Validators.required],
      password: [null, Validators.required],
    });
  }

  registerByEmail(): void {
    for (const i in this.formRegister.controls) {
      if (this.formRegister.controls.hasOwnProperty(i)) {
        this.formRegister.controls[i].markAsDirty();
        this.formRegister.controls[i].updateValueAndValidity();
      }
    }
    if (this.formRegister.valid) {
      this.socket.registerWithEmail(this.formRegister.value);
    }
  }
}
