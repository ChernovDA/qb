import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppGame} from '../../interfaces/app-game';
import {Subscription} from 'rxjs';
import {SocketService} from '../../socket.service';
import {AuthService} from '@auth0/auth0-angular';
import {SocketAuthService} from '../../socket-auth.service';
import {SocketSettingsService} from '../../socket-settings.service';
import {UserRole} from '../../interfaces/user-roles';
import {UserAccount} from '../../interfaces/user-account';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit, OnDestroy {
  upcomingGamesList: Array<AppGame> = [];
  appGamesSubscription: Subscription;
  rolesSubscription: Subscription;
  userAccountSubscription: Subscription;

  roles: UserRole[] = null;
  userAccount: UserAccount = null;

  constructor(public socketService: SocketService,
              public socketAuth: SocketAuthService,
              public socketSettings: SocketSettingsService,
              public auth0: AuthService,
              public router: Router) {
    this.socketService.getAppGames();
    this.socketSettings.getRoles();
  }

  ngOnInit(): void {
    const currentDateTimestamp = Math.floor(Date.now() / 1000);

    this.appGamesSubscription = this.socketService.appGames.subscribe((apps) => {
      this.upcomingGamesList = apps
        .filter((game) => Math.floor(Date.parse(game.nextGameData) / 1000) > currentDateTimestamp);
    });

    // this.rolesSubscription = this.socketSettings.roles.subscribe((roles) => {
    //   this.roles = roles;
    // });
    //
    // this.userAccountSubscription = this.socketAuth.userInfo.subscribe((account) => {
    //   this.userAccount = account;
    //   if (this.roles !== null) {
    //     this.userAccount.role = this.roles.filter((role) => role.id === account.role)[0];
    //   }
    // });

    this.userAccountSubscription = this.socketAuth.userInfo.subscribe((account) => {
      this.rolesSubscription = this.socketSettings.roles.subscribe((roles) => {
        this.roles = roles;
        this.userAccount = account;
        this.userAccount.userRole = this.roles.filter((role) => role.id === account.role)[0];

        if (this.userAccount.role === 4) {
          this.router.navigate(['/playground']);
        }
      });
    });
  }

  ngOnDestroy(): void {
    if (this.appGamesSubscription !== undefined) {
      this.appGamesSubscription.unsubscribe();
    }
    if (this.rolesSubscription !== undefined) {
      this.rolesSubscription.unsubscribe();
    }
    if (this.userAccountSubscription !== undefined) {
      this.userAccountSubscription.unsubscribe();
    }
  }

  registerUser(): void {
    this.auth0.loginWithPopup({screen_hint: 'signup'}).subscribe((result) => {
      this.auth0.user$.subscribe((profile) => {
        this.socketAuth.registerWithEmail({
          email: profile.email,
          username: profile.nickname,
          userRole: this.roles.find((role) => role.isdefault),
          role: this.roles.find((role) => role.isdefault).id,
          avatar: profile.picture
        });
      });
    });
  }
}
