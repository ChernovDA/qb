import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {FbService} from '../../../../fb.service';
import {GameInfo} from '../../../../interfaces/game-info';
import {NzButtonSize} from 'ng-zorro-antd/button';
import {Subscription} from 'rxjs';
import {GameTemplate} from '../../../../interfaces/game-template';
import {SocketService} from '../../../../socket.service';
import {AppGame} from '../../../../interfaces/app-game';

@Component({
  selector: 'app-bd-import',
  templateUrl: './bd-import.component.html',
  styleUrls: ['./bd-import.component.less']
})
export class BdImportComponent implements OnInit {
  ifsGetStages: Subscription;
  ifsGetPortals: Subscription;
  ifsGetQuestions: Subscription;
  currentStatus: string;

  app: Array<AppGame> = [];
  gamesList: Array<GameInfo> = [];
  makedGames: Array<AppGame> = [];
  size: NzButtonSize = 'default';
  stages: GameTemplate[];
  canImport: boolean;
  isSpinning: boolean;
  canImportAppList: boolean;

  gameCode: string;

  constructor(public fbService: FbService, public socket: SocketService) {
    this.canImport = false;
    this.canImportAppList = false;
    this.isSpinning = false;
  }

  ngOnInit(): void {
    this.getGamesList();
    this.getPreviousAppList();
    this.getMakedGames();
  }

  getPreviousAppList(): void {
    this.fbService.gamesListObs.subscribe((games) => {
      this.app = games
        .sort((a, b) => {
          if (a.nextGameData < b.nextGameData) {
            return 1;
          }
          if (a.nextGameData > b.nextGameData) {
            return -1;
          }
          return 0;
        })
      ;
      this.canImportAppList = true;
    });
  }

  getGamesList(): void {
    this.fbService.gamesTitlesObs.subscribe((games) => {
      this.gamesList = games.filter(game => game.id !== 'list');
    });
  }

  getMakedGames(): void {
    this.fbService.import_fromFirestore_GetPastAppGames();

    this.fbService.makedGames.subscribe((games) => {
      let t;
      this.makedGames = games.map((game) => {
        t = new Date(game.nextGameData['seconds'] * 1000);
        game.nextGameData = `${t.getFullYear()}-` +
                            `${t.getMonth() + 1 < 10 ? '0' + (t.getMonth() + 1) : (t.getMonth() + 1)}-` +
                            `${t.getDate() < 10 ? '0' + t.getDate() : t.getDate()} ` +
                            `${t.getHours()}:` +
                            `${t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes()}:` +
                            `${t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds()}`
        ;
        return game;
      });
    });
  }

  calcData(): void {
    this.isSpinning = true;
    this.currentStatus = 'Получаем данные об уровнях';

    this.fbService.import_fromFirestore_GetStages(this.gameCode);

    if (this.ifsGetStages !== undefined) {
      this.ifsGetStages.unsubscribe();
    }
    this.ifsGetStages = this.fbService.ifs_GetStages.subscribe((stages) => {
      this.stages = stages;
      this.currentStatus = 'Получаем данные о порталах на уровнях';
      this.makeGameTemplatePortals(0);
    });
  }

  makeGameTemplatePortals(stage: number): void {
    if (this.stages.hasOwnProperty(stage)) {
      this.fbService.import_fromFirestore_GetPortals(this.gameCode, this.stages[stage].stage.replace('/', '_'));
      if (this.ifsGetPortals !== undefined) {
        this.ifsGetPortals.unsubscribe();
      }
      this.ifsGetPortals = this.fbService.ifs_GetPortals.subscribe((portals) => {
        this.stages[stage].portals = portals;
        if (stage < this.stages.length) {
          this.makeGameTemplatePortals(++stage);
        }

        if (stage === this.stages.length) {
          this.currentStatus = 'Получаем данные о вопросах на уровнях';
          this.makeGameTemplateQuestions(0);
        }
      });
    }
  }
  makeGameTemplateQuestions(stage: number): void {
    if (this.stages.hasOwnProperty(stage)) {
      this.fbService.import_fromFirestore_GetQuestions(this.gameCode, this.stages[stage].stage.replace('/', '_'));
      if (this.ifsGetQuestions !== undefined) {
        this.ifsGetQuestions.unsubscribe();
      }
      this.ifsGetQuestions = this.fbService.ifs_GetQuestions.subscribe((questions) => {
        this.stages[stage].questions = questions;
        if (stage < this.stages.length) {
          this.makeGameTemplateQuestions(++stage);
        }

        if (stage === this.stages.length) {
          this.canImport = true;
          this.isSpinning = false;
          if (this.ifsGetStages !== undefined) {
            this.ifsGetStages.unsubscribe();
          }
        }
      });
    }
  }

  makePastGamePortals(gameIndex: number, stageIndex: number): void {
    if (this.ifsGetPortals !== undefined) {
      this.ifsGetPortals.unsubscribe();
    }
    console.log(this.makedGames[gameIndex]);
    console.log(gameIndex);
    this.fbService.import_fromFirestore_GetPastGamePortals(
      this.makedGames[gameIndex].activeGame,
      this.makedGames[gameIndex].id,
      this.stages[stageIndex].stage.replace('/', '_')
    );
    this.ifsGetPortals = this.fbService.ifs_GetPortals.subscribe((portals) => {
      this.stages[stageIndex].portals = portals;
      if (stageIndex < this.stages.length - 1) {
        this.makePastGamePortals(gameIndex, ++stageIndex);
      }
      else
      if (stageIndex === this.stages.length - 1) {
        this.makePastGameQuestions(gameIndex, 0);
      }

    });
  }
  makePastGameQuestions(gameIndex: number, stageIndex: number): void {
    if (this.ifsGetQuestions !== undefined) {
      this.ifsGetQuestions.unsubscribe();
    }

    this.fbService.import_fromFirestore_GetPastGameQuestions(
      this.makedGames[gameIndex].activeGame,
      this.makedGames[gameIndex].id,
      this.stages[stageIndex].stage.replace('/', '_')
    );
    this.ifsGetQuestions = this.fbService.ifs_GetQuestions.subscribe((questions) => {
      this.stages[stageIndex].questions = questions;
      if (stageIndex < this.stages.length - 1) {
        this.makePastGameQuestions(gameIndex, ++stageIndex);
      }
      else
      if (stageIndex === this.stages.length - 1) {
        // this.makePastGameQuestions(gameIndex, 0);
        console.log(`game done: ${this.makedGames[gameIndex].activeGame} @ ${this.makedGames[gameIndex].nextGameData}`);
        this.socket.insertPastGame(this.stages, this.makedGames[gameIndex].activeGame, this.makedGames[gameIndex].nextGameData);
        if (gameIndex < this.makedGames.length - 1) {
          ++gameIndex;
          this.fbService.import_fromFirestore_GetPastGames(this.makedGames[gameIndex].activeGame, this.makedGames[gameIndex].id);

          if (this.ifsGetStages !== undefined) {
            this.ifsGetStages.unsubscribe();
          }
          this.ifsGetStages = this.fbService.ifs_GetStages.subscribe((stages) => {
            this.stages = stages;

            this.makePastGamePortals(gameIndex, 0);
          });
        }
      }

    });
  }


  importData(): void {
    this.socket.insertToTable(this.stages, this.gameCode);
  }

  importAppList(): void {
    this.socket.insertAppToTable(this.app);
  }

  importMakedGamesList(): void {
    if (this.makedGames.length) {
      this.fbService.import_fromFirestore_GetPastGames(this.makedGames[0].activeGame, this.makedGames[0].id);

      if (this.ifsGetStages !== undefined) {
        this.ifsGetStages.unsubscribe();
      }
      this.ifsGetStages = this.fbService.ifs_GetStages.subscribe((stages) => {
          this.stages = stages;

          this.makePastGamePortals(0, 0);
      });
    }
  }
}
