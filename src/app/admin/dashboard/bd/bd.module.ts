import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzListModule } from 'ng-zorro-antd/list';

import { BdStatsComponent } from './bd-stats/bd-stats.component';
import { BdImportComponent } from './bd-import/bd-import.component';
import { BdExportComponent } from './bd-export/bd-export.component';

import {
  CalculatorOutline,
  DownloadOutline
} from '@ant-design/icons-angular/icons';
import {IconDefinition} from '@ant-design/icons-angular';

const icons: IconDefinition[] = [
  CalculatorOutline,
  DownloadOutline
];

@NgModule({
  declarations: [BdStatsComponent, BdImportComponent, BdExportComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: BdStatsComponent
      },
      {
        path: 'import', component: BdImportComponent
      },
      {
        path: 'export', component: BdExportComponent
      }
    ]),
    NzLayoutModule,
    NzGridModule,
    NzSelectModule,
    NzFormModule,
    FormsModule,
    NzButtonModule,
    NzIconModule.forChild(icons),
    NzSpinModule,
    NzAlertModule,
    NzListModule
  ],
  exports: [
    RouterModule
  ]
})
export class BdModule { }
