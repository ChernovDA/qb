export interface GameInfo {
  id: string;
  title: string;
  type?: string;
}
