export interface ShortGamesListInterface {
  id: number;
  gamedata: string;
  gametitle: string;
  gamecode: string;
  gamestate: string;
  countportals: number;
  countquestions: number;
  countstages: number;
  mapsrc: string;

  canPlay?: boolean; // используется для проверки на вход или необходимость регистрации в игре
  showTimer?: boolean;
}
