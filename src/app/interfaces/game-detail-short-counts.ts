export interface GameDetailsShortQuestions {
  code: string;
  title: string;
  countquestions: string;
}

export interface GameDetailsShortPortals {
  code: string;
  title: string;
  countportals: string;
}
