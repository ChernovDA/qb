import {GameType} from './game-type';

export interface TemplateQuestionInterface {
  id: number;
  available: boolean;
  code: string;
  icon: string;
  iconHeight?: number;
  iconWidth?: number;
  picture?: string | null;
  posX: number;
  posY: number;
  question: string;
  answer: string;
  answers?: string[];
  score: number;
  empty?: boolean;
  busy: boolean;
}

export interface TemplatePortalInterface {
  id: number;
  available: boolean;
  icon: string;
  moveTo: string;
  moveFrom: string;
  posX: number;
  posY: number;
}

export interface GameTemplateMainInfoInterface {
  id: number;
  code: string;
  title: string;
  picture: string | null;
  typeid: number;
  typetitle?: string;
  typecode?: string;
  gameType: GameType;
}
