export interface Question {
  available: boolean;
  answer: string;
  icon: number;
  iconHeight: number;
  iconWidth: number;
  id: string;
  picture: string;
  posX: number;
  posY: number;
  question: string;
  score: number | string;
}
