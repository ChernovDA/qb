export interface GameType {
  id: number;
  title: string;
  code: string;
}
