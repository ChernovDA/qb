export interface GameStateInterface {
  active: boolean;
  announce: boolean;
  gameOver: boolean;
  preparing: boolean;
}

export interface AppGame {
  activeGame: string;
  authModule: string;
  id: string;
  nextGameData?: string;
  profileModule: string;
  state: GameStateInterface | number;
  title?: string;
  code?: string;
}
