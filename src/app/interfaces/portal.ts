export interface Portal {
  available: boolean;
  delayWarp: number;
  icon: number;
  id: number;
  moveTo: string;
  posX: number;
  posY: number;
  stage: string;
}
