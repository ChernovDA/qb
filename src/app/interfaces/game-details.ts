export interface GameDetailsInterface {
  id: number;
  gamedata: string;
  gametitle: string;
  gamecode: string;
  gamestate: string;
  countportals: number;
  countquestions: number;
  countstages: number;
  mapsrc: string;
}
