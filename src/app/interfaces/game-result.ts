export interface GameResultQuestions {
  success: number;
  failed: number;
  total: number;
}

export interface GameResultAnswers {
  success: number;
  total: number;
  quick?: string;
  slow?: string;
}

export interface GameResult {
  userid: number;
  username: string;
  score: number;
  success: number;
  failed: number;
}
