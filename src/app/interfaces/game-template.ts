import {Portal} from './portal';
import {Question} from './question';

export interface GameTemplate {
  map: {
    posX: number;
    posY: number;
    src: string;
  };
  portalIcon: number;
  questionIcon: number;
  stage?: string;
  stageModule?: string;
  title?: string;
  portals?: Portal[];
  questions?: Question[];
  makedGames?: number;
}
