export interface ProfileAuth {
  email: string;
  username: string;
  password: string;
  nickname?: string;
}
