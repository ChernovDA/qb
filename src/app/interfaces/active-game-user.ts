export interface ActiveGameUser {
  score: number;
  id: number;
  role: string;
  username: string;
  email: string;
  avatar: string;
  group: null | string;
  gameMapTitle: string;
  question: string | null;
}
