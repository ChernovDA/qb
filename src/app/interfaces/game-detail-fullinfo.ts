export interface GameDetailsQuestionsFull {
  available: boolean;
  icon: number;
  iconHeight: number;
  iconWidth: number;
  picture: string;
  posX: number;
  posY: number;
  question: string;
  answer: string;
  answers: string[];
  score: number;
  stageModule: number;
  questionIcon: number;
}

export interface GameDetailsPortalsFull {
  available: boolean;
  delayWarp: number;
  icon: number;
  moveTo: string;
  posX: number;
  posY: number;
  stageModule: number;
}
