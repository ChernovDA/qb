export interface MapInterface {
  src: string;
  posX: number;
  posY: number;
}

export interface StageModuleInterface {
  code: string;
  title: string;
  description: string;
  uuid: string;
}

export interface TemplateStageDetailInterface {
  stageID: number;
  stageCode: string;
  stageTitle: string;
  stageModule: StageModuleInterface;
  map: MapInterface;
  portalIcon: string;
  questionIcon: string;
}
