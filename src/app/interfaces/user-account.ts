import {UserRole} from './user-roles';

// "nickname": "chernovda",
// "name": "chernovda@it-murman.ru",
// "picture": "https://s.gravatar.com/avatar/0ffebe3d8f12fef7af8f308345b5b584?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Fch.png",
// "updated_at": "2021-05-13T20:14:06.352Z",
// "email": "chernovda@it-murman.ru",
// "email_verified": false,
// "sub": "auth0|609d888ec4cd5a0069731c99"

export interface GameUserData {
  id: number;
  refApp: number;
  refStage: number;
  refUser: number|string;
  refQuestion?: number|null;
  score: number;
  username?: string;
}

export interface UserAccount {
  id?: string|number;
  role: number;
  userRole?: UserRole;
  username: string;
  email?: string;
  activeGame?: number; // id игры, в которой находится в текущий момент
  group?: number; // id команды, в которую входит участник
  avatar?: string;
  gameData?: GameUserData;
}
