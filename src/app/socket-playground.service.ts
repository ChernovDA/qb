import { Injectable } from '@angular/core';
import { io, Socket } from 'socket.io-client';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs';
import {GameUserData} from './interfaces/user-account';
import {TemplateQuestionInterface} from './interfaces/game-template-main-info';
import {GameStateInterface} from './interfaces/app-game';
import {GameResult, GameResultAnswers, GameResultQuestions} from './interfaces/game-result';

export interface PlayableTime {
  start: number;
  over: number;
}

@Injectable({
  providedIn: 'root'
})
export class SocketPlaygroundService {
  socket: Socket;
  timerCountdown: ReturnType<typeof setInterval>;
  playRemain: number;
  playableTime: PlayableTime;

  constructor() {
    this.socket = io(`${environment.socketScheme}://${environment.socketHost}:${environment.socketPort}`);

    this.timerCountdown = setInterval(() => {
      this.socket.emit('app-activegame-countdown');
      this.socket.on('app-activegame-countdown', (countdown) => {
        if (countdown[0] !== undefined) {
          let time = new Date().getTime() - new Date(countdown[0].nextGameData).getTime();
          if (time < 0) {
            time = new Date(countdown[0].nextGameData).getTime() - new Date().getTime();
          }
          this.playRemain = Number(time.toString()) + 1000 * 60 * 60;
          if (this.playRemain <= 0) {
            clearInterval(this.timerCountdown);
          }
        }
      });
    }, 1000);
  }

  startGameStateListener(): Observable<GameStateInterface> {
    return new Observable<GameStateInterface>((observer) => {
      this.socket.on('app-change-state', (gameID: string, state: string) => {
        state = state.slice(state.indexOf(':') + 1);
        observer.next({
          active: state === '1',
          announce: state === '2',
          gameOver: state === '3',
          preparing: state === '4',
        });
      });
    });
  }

  stopGame(app: number): void {
    this.socket.emit('game-change-state', app, 3);
  }

  playGameCountdown(): Observable<PlayableTime> {
    this.socket.emit('app-activegame-countdown');
    return new Observable<PlayableTime>(observer => {
      this.socket.on('app-activegame-countdown', (countdown) => {
        let timeStart = new Date().getTime() - new Date(countdown[0].nextGameData).getTime();
        const timeOver = new Date(countdown[0].nextGameData).getTime() + 1000 * 60 * 60;
        if (timeStart < 0) {
          timeStart = new Date(countdown[0].nextGameData).getTime() - new Date().getTime();
        }
        this.playableTime = {
          start: Date.now() + Number(timeStart.toString()),
          over: timeOver
        };
        observer.next(this.playableTime);
      });
    });
  }

  addUserToGame(app: number, user: number|string): void {
    this.socket.emit('add-user-togame', app, user);
  }

  get newUserData(): Observable<number> {
    return new Observable<number>((observer) => {
      this.socket.on('add-user-togame', (id) => {
        observer.next(id);
      });
    });
  }

  getGameUserData(gameID: number, userID: number|string): void {
    this.socket.emit('get-game-userdata', gameID, userID);
  }
  get gameUserData(): Observable<GameUserData> {
    return new Observable<GameUserData>((observer) => {
      this.socket.on('get-game-userdata', (gameUserData) => {
        observer.next(gameUserData);
      });
    });
  }

  getAllGamesUsersData(): void {
    this.socket.emit('get-all-games-userdata');
  }
  get allGamesUsersData(): Observable<GameUserData[]> {
    return new Observable<GameUserData[]>((observer) => {
      this.socket.on('get-all-games-userdata', (gamesUsersData) => {
        observer.next(gamesUsersData);
      });
    });
  }

  getGameAllUsersData(gameID: number): void {
    this.socket.emit('get-game-allusersdata', gameID);
  }
  get gameAllUsersData(): Observable<GameUserData[]> {
    return new Observable<GameUserData[]>((observer) => {
      this.socket.on('get-game-allusersdata', (gameAllUsersData) => {
        observer.next(gameAllUsersData);
      });
    });
  }

  getGameOverUsersData(gameID: number): void {
    this.socket.emit('get-gameover-userdata', gameID);
  }
  get gameOverUsersData(): Observable<GameResult[]> {
    return new Observable<GameResult[]>((observer) => {
      this.socket.on('get-gameover-userdata', (data: GameResult[]) => {
        observer.next(data.map((result) => {
          result.success = result.success === null ? 0 : result.success;
          result.failed = result.failed === null ? 0 : result.failed;
          return result;
        }));
      });
    });
  }

  getGameOverUsersQuestions(gameID: number): void {
    this.socket.emit('get-gameover-stat-questions', gameID);
  }
  get gameOverUsersQuestions(): Observable<GameResultQuestions> {
    return new Observable<GameResultQuestions>((observer) => {
      this.socket.on('get-gameover-stat-questions', (data: GameResultQuestions) => {
        observer.next(data);
      });
    });
  }

  getGameOverUsersAnswers(gameID: number): void {
    this.socket.emit('get-gameover-stat-answers', gameID);
  }
  get gameOverUsersAnswers(): Observable<GameResultAnswers> {
    return new Observable<GameResultAnswers>((observer) => {
      this.socket.on('get-gameover-stat-answers', (data: GameResultAnswers) => {
        observer.next(data);
      });
    });
  }

  updateUserStage(gameID: number, userID: number|string, stage: number): void {
    this.socket.emit('update-game-userdata-stage', gameID, userID, stage);
  }

  updateBusyStatus(questionID: number, userID: number|string|null): void {
    this.socket.emit('update-question-busy', questionID, userID);
  }

  updateUserScores(gameID: number, userID: number|string, score: number): void {
    this.socket.emit('update-user-scores', gameID, userID, score);
  }

  toggleEmpty(questionID: number): void {
    this.socket.emit('update-question-empty', questionID);
  }

  logTryAnswer(gameID: number, questionID: number, userID: number|string, valid: boolean, answer: string): void {
    const d = new Date();
    const date = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`;
    const time = `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
    this.socket.emit('log-try-answer', gameID, questionID, userID, valid, answer, date, time);
  }

  updateQuestion(gameID: number, question: TemplateQuestionInterface): void {
    // TODO: Смена иконки
    question.icon = '2';
    this.socket.emit('update-game-question', gameID, question);
  }

  get listenUpdateQuestionData(): Observable<TemplateQuestionInterface> {
    return new Observable<TemplateQuestionInterface>((observer) => {
      this.socket.on('update-game-question', (question: TemplateQuestionInterface) => {
        observer.next(question);
      });
    });
  }

  get listenScoreChanges(): Observable<GameUserData> {
    return new Observable<GameUserData>((observer) => {
      this.socket.on('change-users-scores', (userData) => {
        observer.next(userData);
      });
    });
  }
}
