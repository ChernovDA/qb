import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '@auth0/auth0-angular';
import {SocketAuthService} from './socket-auth.service';
import {Subscription, throwError} from 'rxjs';
import {UserRole} from './interfaces/user-roles';
import {SocketSettingsService} from './socket-settings.service';
import {NavigationEnd, Router} from '@angular/router';
import {UserAccount} from './interfaces/user-account';
import firebase from 'firebase';
import auth = firebase.auth;
import {ProfileAuth} from './interfaces/profile-auth';
import {catchError} from 'rxjs/operators';
import {NzNotificationService} from 'ng-zorro-antd/notification';
import {NzModalService} from 'ng-zorro-antd/modal';
import {RegisterComponent} from './profile/auth/register/register.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, OnDestroy {
  roles: UserRole[] = null;
  registerState: boolean;
  userAccountSubscription?: Subscription;
  rolesSubscription?: Subscription;
  tryRegisterSubscription?: Subscription;
  registerFormSubscription?: Subscription;
  isLogged: boolean;
  needBeAuthed: boolean;
  viewRegisterForm: boolean;

  constructor(public auth0: AuthService,
              public socketSettings: SocketSettingsService,
              public socketAuth: SocketAuthService,
              private router: Router,
              private notification: NzNotificationService,
              private modal: NzModalService,
              ) {
    this.socketSettings.getRoles();
    this.viewRegisterForm = false;

  }

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/demo') {
          this.needBeAuthed = false;
        } else {
          this.needBeAuthed = true;
          this.rolesSubscription = this.socketSettings.roles.subscribe((roles) => {
            this.roles = roles;
          });
          this.userAccountSubscription = this.socketAuth.userInfo.subscribe((account: UserAccount) => {
            this.isLogged = (typeof account === 'object');
            this.registerState = !this.isLogged;
            this.auth0.isAuthenticated$.subscribe((authed) => {
              if (authed && this.registerState === false && this.isLogged) {
                if (this.router.url === '/') {
                  switch (account.role) {
                    case 1:
                    case 2:
                    case 3: {
                      this.router.navigate(['admin', 'dashboard']);
                      break;
                    }
                    default: {
                      this.router.navigate(['playground', 'games-list']);
                    }
                  }
                }
              }
            });
          });
        }
        // console.log('need be authed:', this.needBeAuthed);
      }
    });
  }

  ngOnDestroy(): void {
    this.tryRegisterSubscription?.unsubscribe();
    this.userAccountSubscription?.unsubscribe();
    this.rolesSubscription?.unsubscribe();
    this.registerFormSubscription?.unsubscribe();
  }

  loginUser(): void {
    this.registerState = false;
    this.auth0.loginWithPopup({screen_hint: 'login'});
  }

  registerUser(): void {
    const modal = this.modal.create({
      nzContent: RegisterComponent,
      nzFooter: '',
      nzClosable: false,
      nzWidth: 360,
      nzCentered: true
    });

    this.registerFormSubscription = modal.afterClose.subscribe((result) => {
      if (result !== undefined) {
        this.tryRegister(result.user);
      }
    });

    // this.registerState = true;
    //
    // this.socketAuth.resultRegisterWithEmail.subscribe((result) => {
    //   console.log(`registered: ${result}`);
    //   if (result === true) {
    //     this.router.navigate(['/admin/dashboard']);
    //   }
    // });
    //
    // this.auth0.loginWithPopup({screen_hint: 'signup'}).subscribe((result) => {
    //   this.auth0.user$.subscribe((profile) => {
    //     this.socketAuth.registerWithEmail({
    //       email: profile.email,
    //       username: profile.nickname,
    //       role: this.roles.find((role) => role.isdefault).id,
    //       userRole: this.roles.find((role) => role.isdefault),
    //       avatar: profile.picture
    //     });
    //   });
    // });
  }

  tryRegister(authFields: ProfileAuth): void {
    this.tryRegisterSubscription = this.socketAuth.apiRegister(authFields)
      .pipe(
        catchError((error) => {
          switch (error.error.code) {
            case 'invalid_signup': { // предположительно такой аккаунт уже зарегистрирован
              this.notification.error(
                'Ошибка регистрации',
                'Аккаунт уже существует. Попробуй-те использовать другой email'
              );
              break;
            }
          }

          return throwError(error);
        })
      )
      .subscribe((profile) => {
        this.socketAuth.registerWithEmail({
          email: profile.email,
          username: profile.username,
          role: this.roles.find((role) => role.isdefault).id,
          userRole: this.roles.find((role) => role.isdefault),
          avatar: '/assets/avatars/profile-template.jpg'
        });

        this.notification.success(
          'Пользователь создан',
          'Вы успешно создали аккаунт в система. Пожалуйста, теперь авторизуйтесь'
        );

        this.auth0.loginWithPopup({screen_hint: 'login'});
        this.tryRegisterSubscription?.unsubscribe();
    });
  }
}
