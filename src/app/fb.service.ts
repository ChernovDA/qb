import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';
import {BehaviorSubject, Observable} from 'rxjs';
import {AppGame} from './interfaces/app-game';
import {map} from 'rxjs/operators';
import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;
import {GameInfo} from './interfaces/game-info';
import {GameTemplate} from './interfaces/game-template';
import {Portal} from './interfaces/portal';
import {Question} from './interfaces/question';
import App = firebase.app.App;

@Injectable({
  providedIn: 'root'
})
export class FbService {
  gamesList: Observable<AppGame[]>;
  gamesTitles: Observable<GameInfo[]>;
  makedGames: Observable<AppGame[]>;
  ifsGameStages: Observable<GameTemplate[]>;
  ifsGameStagePortals: Observable<Portal[]>;
  ifsGameStageQuestions: Observable<Question[]>;

  constructor(public firestore: AngularFirestore) {
    this.bindGamesList();
    this.loadGamesAvailable();
  }

  bindGamesList(): void {
    this.gamesList = this.firestore.collection<AppGame>('app').snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as AppGame;
        Object.keys(data).filter(key => data[key] instanceof Timestamp)
          .forEach(key => data[key] = data[key].toDate());
        const id = a.payload.doc.id;
        return {id, ...data};
      }))
    );
  }

  get gamesListObs(): Observable<AppGame[]> {
    return new Observable<AppGame[]>(fn => this.gamesList.subscribe(fn));
  }

  loadGamesAvailable(): void {
    this.gamesTitles = this.firestore.collection<GameInfo>('games').snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as GameInfo;
        const id = a.payload.doc.id;
        const title = data.title;
        return {id, title, ...data};
      }))
    );
  }

  get gamesTitlesObs(): Observable<GameInfo[]> {
    return new Observable<GameInfo[]>(fn => this.gamesTitles.subscribe(fn));
  }

  addGameToSchedule(game: AppGame): Promise<void> {
    return this.firestore.doc<AppGame>(`app/${game.id}`).set(game);
  }

  setGameState(gameUUID: string, state: {
    announce: boolean,
    preparing: boolean,
    active: boolean,
    gameOver: boolean
  }): Promise<void> {
    return this.firestore.doc<AppGame>(`app/${gameUUID}`).update({
      state
    });
  }

  import_fromFirestore_GetStages(gameID: string): void {
    this.ifsGameStages = this.firestore.collection<GameTemplate>(`games/${gameID}/stages`).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as GameTemplate;
        const id = a.payload.doc.id;
        return {id, ...data};
      }))
    );
  }

  get ifs_GetStages(): Observable<GameTemplate[]> {
    return new Observable<GameTemplate[]>(fn => this.ifsGameStages.subscribe(fn));
  }

  import_fromFirestore_GetPortals(gameID: string, stageID: string): void {
    this.ifsGameStagePortals = this.firestore.collection<Portal>(`games/${gameID}/stages/${stageID}/portals`).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Portal;
        const id = a.payload.doc.id;
        return {id, ...data};
      }))
    );
  }

  import_fromFirestore_GetPastGamePortals(gameID: string, pastGameUUID: string, stageID: string): void {
    this.ifsGameStagePortals = this.firestore.collection<Portal>(`games/${gameID}/${pastGameUUID}/${stageID}/portals`).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Portal;
        const id = a.payload.doc.id;
        return {id, ...data};
      }))
    );
  }

  get ifs_GetPortals(): Observable<Portal[]> {
    return new Observable<Portal[]>(fn => this.ifsGameStagePortals.subscribe(fn));
  }

  import_fromFirestore_GetQuestions(gameID: string, stageID: string): void {
    this.ifsGameStageQuestions = this.firestore.collection<Question>(`games/${gameID}/stages/${stageID}/questions`).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Question;
        const id = a.payload.doc.id;
        return {id, ...data};
      }))
    );
  }

  import_fromFirestore_GetPastGameQuestions(gameID: string, pastGameUUID: string, stageID: string): void {
    this.ifsGameStageQuestions = this.firestore.collection<Question>(`games/${gameID}/${pastGameUUID}/${stageID}/questions`).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Question;
        const id = a.payload.doc.id;
        return {id, ...data};
      }))
    );
  }

  get ifs_GetQuestions(): Observable<Question[]> {
    return new Observable<Question[]>(fn => this.ifsGameStageQuestions.subscribe(fn));
  }

  import_fromFirestore_GetPastAppGames(): void {
    // this.ifsGameStages = this.firestore.collection<GameTemplate>(`games/${gameCode}/${gameID}`).snapshotChanges().pipe(
    this.makedGames = this.firestore.collection<AppGame>(`app`).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as AppGame;
        const id = a.payload.doc.id;
        data.id = data.id.replace(' ', '');
        if (data.id === '0d0gMkCuixDSlwZa96MG') {
          data.activeGame = 'HP';
        }
        if (data.id === 'JWwqw8HB8INXNd5qSTxc') {
          data.activeGame = 'HP';
        }
        if (data.id === 'tUBcvOk1P6LUYb8tGCKv') {
          data.activeGame = 'HOLMES';
        }

        return {id, ...data};
      }))
    );
  }

  import_fromFirestore_GetPastGames(gameCode: string, gameUUID: string): void {
    this.ifsGameStages = this.firestore.collection<GameTemplate>(`games/${gameCode}/${gameUUID}/`).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as GameTemplate;
        const id = a.payload.doc.id;
        return {id, ...data};
      }))
    );
  }
}
