// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    // apiKey: 'AIzaSyCpWkjUNp19oqNXEBiZMVWT9i9F-miCjFA',
    // authDomain: 'questbook3d.firebaseapp.com',
    // databaseURL: 'https://questbook3d.firebaseio.com',
    // projectId: 'questbook3d',
    // storageBucket: 'questbook3d.appspot.com',
    // messagingSenderId: '303256240951',
    // appId: '1:303256240951:web:ffc62995b88704177990b2'

    apiKey: 'AIzaSyAUUrgqHT3Zu8_7gPydZe2lUCJRVKCKVsk',
    authDomain: 'questbook-dev.firebaseapp.com',
    databaseURL: 'https://questbook-dev.firebaseio.com',
    projectId: 'questbook-dev',
    storageBucket: 'questbook-dev.appspot.com',
    messagingSenderId: '618180324560',
    appId: '1:618180324560:web:b4ca59e937944c8becc2d5'
  },
  useEmulators: false,
  socketScheme: 'http',
  socketHost: '192.168.100.4',
  socketPort: '8008',
  socketPortPG: '8009'
  // socketScheme: 'https',
  // socketHost: 'qb.ratatosk.studio',
  // socketPort: '8008',
  // socketPortPG: '8009'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

