export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCpWkjUNp19oqNXEBiZMVWT9i9F-miCjFA',
    authDomain: 'questbook3d.firebaseapp.com',
    databaseURL: 'https://questbook3d.firebaseio.com',
    projectId: 'questbook3d',
    storageBucket: 'questbook3d.appspot.com',
    messagingSenderId: '303256240951',
    appId: '1:303256240951:web:ffc62995b88704177990b2'
  },
  useEmulators: false,
  socketScheme: 'https',
  socketHost: 'qb.ratatosk.studio',
  socketPort: '8008',
  socketPortPG: '8009'
};
