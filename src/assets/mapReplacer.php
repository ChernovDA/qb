<?php
// postgresql://qb:qb@192.168.100.4:5432/qb
$pgconn = pg_connect("host=192.168.100.4 port=5432 dbname=qb user=qb password=qb");

$res = pg_query($pgconn, "select src from \"stageBackground\"");
$result = pg_fetch_all($res);
$mapsSrc = [];

foreach ($result as $mapSrc):
  $from = strpos($mapSrc["src"], "maps") + 6;
  $to = strpos($mapSrc["src"], "?alt", $from);
  $mapsSrc[] = [
    "original" => $mapSrc["src"],
    "filename" => "/assets/maps/". substr($mapSrc["src"], $from, $to - $from)
  ];
endforeach;

$root = realpath(dirname(__FILE__). "/../");
foreach ($mapsSrc as $src):
  if (!file_exists($root. $src["filename"])) {
    file_put_contents($root. $src["filename"], fopen($src["original"], "r"));
  }
  pg_query_params($pgconn, "update \"stageBackground\" set src = $1 where src = $2", [$src["filename"], $src["original"]]);
endforeach;

pg_close($pgconn);
